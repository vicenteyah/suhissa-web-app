import palette from './palette'

const PRIMARY_COLOR = [
  // DEFAULT
  {
    name: 'default',
    ...palette.light.primary,
  },
  // PURPLE
  {
    name: 'purple',
    lighter: '#EBD6FD',
    light: '#B985F4',
    main: '#7635dc',
    dark: '#431A9E',
    darker: '#200A69',
    contrastText: '#fff',
  },
  // CYAN
  {
    name: 'cyan',
    lighter: '#D1FFFC',
    light: '#76F2FF',
    main: '#1CCAFF',
    dark: '#0E77B7',
    darker: '#053D7A',
    contrastText: palette.light.grey[800],
  },
  // BLUE
  {
    name: 'blue',
    lighter: '#4a5fa4',
    light: '#2d4698',
    main: '#001d7e',
    dark: '#263a7d',
    darker: '#1e2d61',
    contrastText: '#fff',
  },
  // ORANGE
  {
    name: 'orange',
    lighter: '#FEF4D4',
    light: '#FED680',
    main: '#fda92d',
    dark: '#B66816',
    darker: '#793908',
    contrastText: palette.light.grey[800],
  },
  // RED
  {
    name: 'red',
    lighter: '#FFE3D5',
    light: '#FFC1AC',
    main: '#FF3030',
    dark: '#B71833',
    darker: '#7A0930',
    contrastText: '#fff',
  },
]

function SetColor(themeColor: String) {
  let color
  const DEFAULT = PRIMARY_COLOR[0]
  const PURPLE = PRIMARY_COLOR[1]
  const CYAN = PRIMARY_COLOR[2]
  const BLUE = PRIMARY_COLOR[3]
  const ORANGE = PRIMARY_COLOR[4]
  const RED = PRIMARY_COLOR[5]

  switch (themeColor) {
    case 'purple':
      color = PURPLE
      break
    case 'cyan':
      color = CYAN
      break
    case 'blue':
      color = BLUE
      break
    case 'orange':
      color = ORANGE
      break
    case 'red':
      color = RED
      break
    default:
      color = DEFAULT
  }
  return color
}

export default SetColor
