import create from 'zustand'
import remove from 'lodash/remove'
import { persist } from 'zustand/middleware'

interface ITab {
  id: string
  name: string
  tag: string
  componentPath: string
  ContentComponent: React.ComponentType
}

interface ITabsState {
  tabs: ITab[]
  addTab: (tab: ITab) => void
  removeTab: (tabId: string) => void
  setTabComponent: (
    tabId: string,
    ContentComponent: React.ComponentType,
  ) => void
}

interface ISelectedTabState {
  selectedTab: ITab | undefined
  setSelectedTab: (tab: ITab) => void
}

const functionalPush = (array: ITab[], tab: ITab) => {
  array.push(tab)
  return array
}

const useTabsStore = create((set) => ({
  tabs: [],
  addTab: (tab: any) =>
    set(({ tabs }: { tabs: [] }) => ({
      tabs: functionalPush(tabs, tab),
    })),
  removeTab: (tabId: number) =>
    set(({ tabs }: { tabs: [] }) => ({
      tabs: tabs.filter((value: { id: number }) => {
        return value.id !== tabId
      }),
    })),
  setTabComponent: (tabId: number, ContentComponent: any) =>
    set(({ tabs }: { tabs: [] }) => ({
      tabs: tabs.map((tab: { id: number }) =>
        tabId === tab.id ? { ...tab, ContentComponent: ContentComponent } : tab,
      ),
    })),
}))

const useSelectedTabStore = create<ISelectedTabState>((set) => ({
  selectedTab: undefined,
  setSelectedTab: (tab) => set((state) => ({ selectedTab: tab })),
}))

export type { ITab, ISelectedTabState }
export { useSelectedTabStore }
export default useTabsStore
