function path(root: string, sublink: string) {
  return `${root}${sublink}`
}

const ROOTS_DASHBOARD = '/dashboard'
const ROOTS_CONFIGURATION = '/configuration'
const ROOTS_LOGIN = '/login'

export const PATH_LOGIN = {
  root: ROOTS_LOGIN,
}

export const PATH_DASHBOARD = {
  // root: ROOTS_DASHBOARD
  root: path(ROOTS_DASHBOARD, '/wells'),
  general: {
    wells: path(ROOTS_DASHBOARD, '/wells'),
    dashboard: path(ROOTS_DASHBOARD, '/dashboard'),
  },
  configuration: {
    account: path(ROOTS_CONFIGURATION, '/account'),
  },
}
