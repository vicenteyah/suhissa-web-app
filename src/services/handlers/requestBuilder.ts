import { Method } from 'services/lib/fetchFromAPI'

interface IBuildRequest<T extends any> {
  url: string
  method: Method
  requiresAuth?: boolean
  body?: T
}

const buildRequest = <T>(req: IBuildRequest<T>) => ({
  ...req,
  body: JSON.stringify(req.body),
})

export type { IBuildRequest }
export default buildRequest
