import not from 'utils/functions/not'
import { OneParamHandler } from '@customTypes/queries/Handler'
import Predicate from '@customTypes/Predicate'
import UserSession from '@customTypes/dto/UserSession'

type RedirectIf = (
  condition: Predicate<[UserSession]>,
) => (to: string) => OneParamHandler<UserSession>

const redirectIf: RedirectIf =
  (condition) =>
  (to) =>
  async ({ req, res }) => {
    const user = req.session.get('user')

    if (condition(user as UserSession)) {
      res.setHeader('location', to)
      res.statusCode = 302
      res.end()
      return { props: {} }
    }

    return {
      props: { user: req.session.get('user') },
    }
  }

const loggedIn = (user: UserSession) => user !== undefined
const hasAuth = (user: UserSession) =>
  user ? !!user.token !== undefined : false

const redirectIfLoggedIn = redirectIf(loggedIn)
const redirectIfNotLoggedIn = redirectIf(not(loggedIn))
const redirectIfNotAuth = redirectIf(not(hasAuth))

export {
  redirectIf,
  redirectIfLoggedIn,
  redirectIfNotLoggedIn,
  redirectIfNotAuth,
}
