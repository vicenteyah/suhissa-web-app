import UserSession from '@customTypes/dto/UserSession'
import { TLogoutUser } from 'services/factories/UserService/logoutUser'
import { TSignInUser } from 'services/factories/UserService/signInUser'

interface IUserService {
  user: UserSession | undefined
  signInUser: TSignInUser
  logoutUser: TLogoutUser
}

export type { IUserService }
