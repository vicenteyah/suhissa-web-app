interface IEntityById {
  id: number
}

interface INewEntityInstance<Entity> {
  entity: Omit<Entity, 'id'>
}

interface IEditEntityInstance<Entity> extends IEntityById {
  entity: Omit<Entity, 'id'>
}

type TWithMutator<T, Mutator> = ({ mutator }: { mutator?: Mutator }) => T
type TElementMutify<Operations, Mutator> = {
  [operation in keyof Operations]: TWithMutator<Operations[operation], Mutator>
}

type TGetOne<Entity> = ({ id }: IEntityById) => Promise<Entity>
type TUpdate<Entity> = ({
  entity,
  id,
}: IEditEntityInstance<Entity>) => Promise<Entity>
type TCreate<Entity> = ({
  entity,
}: INewEntityInstance<Entity>) => Promise<Entity>
type TDelete<Entity> = ({ id }: IEntityById) => Promise<Entity>

type TServiceCrudOperations<Entity> = {
  getAll: Entity[]
  getOne: TGetOne<Entity>
  update: TUpdate<Entity>
  create: TCreate<Entity>
  delete: TDelete<Entity>
}

type TGenericService<Entity, Mutator = Function> = {
  (): TServiceCrudOperations<Entity> &
    Record<string, any> & { mutator: Mutator }
  withMutator: TElementMutify<TServiceCrudOperations<Entity>, Mutator>
}

type TGenericServiceReturn<T, M> = ReturnType<TGenericService<T, M>>

export type { TGenericService, TGenericServiceReturn }
