const handleFetchError = (res: any) => (error: any) => {
  const { response: fetchResponse } = error

  res.status(fetchResponse?.status || 500).json(error.data)
}

export default handleFetchError
