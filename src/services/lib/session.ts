import { withIronSession } from 'next-iron-session'
import { Handler } from '@customTypes/queries/Handler'
import SessionValue from '@customTypes/queries/SessionValue'

export default function withSession<T extends SessionValue>(
  handler: Handler<T>,
) {
  return withIronSession(handler, {
    password:
      process.env.NEXT_PUBLIC_SECRET_COOKIE_PASSWORD ||
      '798602229600-092445191699-397420830490', // randomly generated numbers, it requires a minimum of 36 characters
    cookieName: 'session-cookie',
    cookieOptions: {
      secure: process.env.NODE_ENV === 'production',
    },
  })
}
