import { GetServerSideProps } from 'next'
import { OneParamHandler } from '@customTypes/queries/Handler'
import SessionValue from '@customTypes/queries/SessionValue'
import withSession from './session'

// This function fixes a nextjs issue described here: https://github.com/vercel/next.js/discussions/11209
export default function getSspWithSession<T extends SessionValue>(
  handler: OneParamHandler<T>,
): GetServerSideProps {
  return async (sspArgs) => {
    await withSession(handler)(sspArgs)

    return {
      props: {
        alertMessage: sspArgs.query.alertMessage || null,
        alertMessageType: sspArgs.query.alertMessageType || null,
        one: 1,
      },
    }
  }
}
