import { IBuildRequest } from 'services/handlers/requestBuilder'
import { Method } from './fetchFromAPI'
import fetchJson from './fetchJson'

// @ts-expect-error fetch expects 1-2 arguments instead of 0 or more
const fetcher = (...args) => fetch(...args).then((res) => res.json())
const fetcherWithRequest = <T extends ReadableStream<Uint8Array> | null>(
  req: Omit<IBuildRequest<T>, 'method'> & { method: Method.GET },
) =>
  fetchJson('/api/single-request', {
    method: Method.POST,
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(req),
  })

export { fetcherWithRequest }
export default fetcher
