import fetchJson from 'services/lib/fetchJson'
import HTTPResponse from '@customTypes/queries/HTTPResponse'
import Req from '@customTypes/queries/Req'
import UserSession from '@customTypes/dto/UserSession'

const baseUrl =
  process.env.NEXT_PUBLIC_API_URL || `http://demo.localhost:8000/api/`

enum Method {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  DELETE = 'DELETE',
  NOTHING = 'NOTHING',
}

interface FetchFromAPIProps {
  method: Method
  requiresAuth: boolean
  url?: string
  body?: any
}

type FetchFromAPI = <T>(
  req: Req<UserSession>,
) => (props: FetchFromAPIProps) => Promise<HTTPResponse<T | 'Nothing changed'>>

const fetchFromAPI: FetchFromAPI =
  (req) =>
  ({ url, method, body, requiresAuth }) => {
    if (method === 'NOTHING') {
      return Promise.resolve({
        status: 200,
        message: 'Nothing changed',
        data: 'Nothing changed',
      })
    }

    const requestInit: RequestInit = {
      method,
      headers: { 'Content-Type': 'application/json' },
    }

    if (body != null) {
      requestInit.body = typeof body === 'object' ? JSON.stringify(body) : body
    }

    if (requiresAuth) {
      const user = req.session.get('user')!
      const token = user.isLoggedIn ? user.token : null

      // requestInit.headers is not of type Headers, but Record<string, string> which is a valid HeadersInit object
      // https://microsoft.github.io/PowerBI-JavaScript/modules/_node_modules_typedoc_node_modules_typescript_lib_lib_dom_d_.html#headersinit
      // @ts-expect-error
      requestInit.headers.Authorization = `Bearer ${token}`
    }

    return fetchJson(`${baseUrl}${url}`, requestInit)
  }

export default fetchFromAPI
export { Method }
export type { FetchFromAPIProps }
