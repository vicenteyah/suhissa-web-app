import UserSession from '@customTypes/dto/UserSession'
import fetcher from 'services/lib/fetcher'
import useSWR from 'swr'
import router from 'next/router'
import { PATH_DASHBOARD } from 'routes/paths'
import { IUserService } from 'services/types/IUserService'
import logoutUser from './logoutUser'
import signInUser from './signInUser'

function useUserService(): IUserService {
  const { data: user, mutate: mutateUser } = useSWR<UserSession>(
    '/api/user',
    fetcher,
  )

  const goToSignIn = () => {
    router.push(PATH_DASHBOARD.general.dashboard)
  }

  return {
    user,
    signInUser: signInUser({ mutateUser }),
    logoutUser: logoutUser({ mutateUser, callback: goToSignIn }),
  }
}

export default useUserService
