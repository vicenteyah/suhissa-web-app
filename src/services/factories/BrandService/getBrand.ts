import { IBrand } from '@customTypes/entities/brand'
import requestBuilder from 'services/handlers/requestBuilder'
import { Method } from 'services/lib/fetchFromAPI'
import fetchJson from 'services/lib/fetchJson'
import { PATH, TBrandService } from '.'

type TGetBrand = TBrandService['withMutator']['getOne']

const getBrand: TGetBrand =
  () =>
  async ({ id }) => {
    const request = requestBuilder({
      url: `${PATH}${id}`,
      method: Method.GET,
      requiresAuth: true,
    })

    const brandToFind: Promise<IBrand> = fetchJson(`/api/single-request/`, {
      method: Method.POST,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(request),
    })

    return brandToFind
  }

export default getBrand
