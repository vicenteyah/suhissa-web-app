import { IBrand } from '@customTypes/entities/brand'
import { fetcherWithRequest } from 'services/lib/fetcher'
import useSWR, { mutate } from 'swr'
import { KeyedMutator } from 'swr/dist/types'
import { Method } from 'services/lib/fetchFromAPI'
import { TGenericService } from 'services/types/TGenericService'
import requestBuilder from '../../handlers/requestBuilder'
import getBrand from './getBrand'
import updateBrand from './updateBrand'
import createBrand from './createBrand'
import deleteBrand from './deleteBrand'

export const PATH = 'brand/'

const getAll = requestBuilder({
  url: PATH,
  method: Method.GET,
  requiresAuth: true,
})

type TBrandService = TGenericService<IBrand, KeyedMutator<IBrand[]>>
type TBrandServiceAPI = Pick<
  ReturnType<TBrandService>,
  'getAll' | 'getOne' | 'update' | 'create' | 'delete' | 'mutator'
>

function useBrandService(): TBrandServiceAPI {
  const { data: brands, mutate } = useSWR<IBrand[]>(
    [getAll],
    fetcherWithRequest,
  )

  return {
    getAll: brands ?? [],
    getOne: getBrand({}),
    update: updateBrand({ mutator: mutate }),
    create: createBrand({ mutator: mutate }),
    delete: deleteBrand({ mutator: mutate }),
    mutator: mutate,
  }
}

export type { TBrandService }
export default useBrandService
