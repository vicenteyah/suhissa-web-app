import { IBrand } from '@customTypes/entities/brand'
import requestBuilder from 'services/handlers/requestBuilder'
import { Method } from 'services/lib/fetchFromAPI'
import fetchJson from 'services/lib/fetchJson'
import { PATH, TBrandService } from '.'

type TGetBrand = TBrandService['withMutator']['create']

const createBrand: TGetBrand =
  ({ mutator }) =>
  async ({ entity }) => {
    const request = requestBuilder({
      url: PATH,
      method: Method.POST,
      requiresAuth: true,
      body: entity,
    })

    const brandToFind: Promise<IBrand> = fetchJson(`/api/single-request/`, {
      method: Method.POST,
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify(request),
    })
    mutator!()
    return brandToFind
  }

export default createBrand
