import { useState } from 'react'

type TSortingType = 'asc' | 'desc'

interface IUsehandleSort {
  defaultRowsPerPage: number
  orderByProperty: string
  defaultSort: TSortingType
  defaultPage: number
}

export default function useHandleSort({
  defaultRowsPerPage = 5,
  orderByProperty = 'name',
  defaultSort = 'asc',
  defaultPage = 0,
}: Partial<IUsehandleSort>) {
  const [page, setPage] = useState(defaultPage)
  const [order, setOrder] = useState<TSortingType>(defaultSort)
  const [orderBy, setOrderBy] = useState(orderByProperty)
  const [rowsPerPage, setRowsPerPage] = useState(defaultRowsPerPage)

  const handleRequestSort = (property: string) => {
    const isAsc = orderBy === property && order === 'asc'

    setOrder(isAsc ? 'desc' : 'asc')
    setOrderBy(property)
  }

  const handleChangeRowsPerPage = (
    event: React.ChangeEvent<HTMLInputElement>,
  ) => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0)
  }

  return {
    page,
    setPage,
    handleRequestSort,
    handleChangeRowsPerPage,
    order,
    rowsPerPage,
    orderBy,
  }
}
