import { useContext } from 'react'
import { CollapseDrawerContext } from '../contexts/CollapseDrawerContent'

const useCollapseDrawer = () => useContext(CollapseDrawerContext)

export default useCollapseDrawer
