import { useSnackbar as useOldSnackbar } from 'notistack5'
import { MIconButton } from 'components/@material-extend'
import closeFill from '@iconify/icons-eva/close-fill'
import { Icon } from '@iconify/react'

export default function useSnackbar() {
  const { enqueueSnackbar, closeSnackbar } = useOldSnackbar()

  const enqueueSuccessSnackbar = ({ message }: { message: string }) =>
    enqueueSnackbar(message, {
      variant: 'success',
      action: (key) => (
        <MIconButton size="small" onClick={() => closeSnackbar(key)}>
          <Icon icon={closeFill} />
        </MIconButton>
      ),
    })

  const enqueueErrorSnackbar = ({ message }: { message: string }) =>
    enqueueSnackbar(message, {
      variant: 'error',
      action: (key) => (
        <MIconButton size="small" onClick={() => closeSnackbar(key)}>
          <Icon icon={closeFill} />
        </MIconButton>
      ),
    })

  return { enqueueSuccessSnackbar, enqueueErrorSnackbar }
}
