import React from 'react'
import { useForm, Controller } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import {
  Button,
  Dialog,
  TextField,
  DialogTitle,
  DialogContent,
  DialogActions,
  DialogContentText,
} from '@material-ui/core'
import { When } from 'react-if'
import useDisclousure from './useDisclousure'

interface IUseDialog {
  title?: string
  description?: string
  schema?: any
  submitActionText: string
  fields: [
    {
      name: string
      label: string
      type: string
      defaultValue?: string
      fullWidth?: boolean
      required?: boolean
    },
  ]
}

export default function useDialog({
  schema,
  title,
  description,
  fields,
  submitActionText,
}: IUseDialog) {
  const { isOpen, onOpen, onClose } = useDisclousure()
  const {
    control,
    handleSubmit,
    reset,
    formState: { errors },
  } = useForm({
    resolver: schema ? yupResolver(schema) : undefined,
  })

  return {
    isOpen,
    onOpen,
    onClose,
    reset,
    handleSubmit,
    component: (onSubmit: VoidFunction) => (
      <Dialog open={isOpen} onClose={onClose} fullWidth maxWidth="sm">
        <When condition={title}>
          <DialogTitle sx={{ my: 2 }}>{title}</DialogTitle>
        </When>
        <DialogContent>
          <When condition={description}>
            <DialogContentText>
              To subscribe to this website, please enter your email address
              here. We will send updates occasionally.
            </DialogContentText>
          </When>
          {fields.map(
            ({
              defaultValue,
              fullWidth,
              name: fieldName,
              type,
              label,
              required,
            }) => (
              <Controller
                key={fieldName}
                defaultValue={defaultValue}
                render={({ field }) => (
                  <TextField
                    {...field}
                    sx={{ my: 1 }}
                    fullWidth={fullWidth}
                    autoComplete={fieldName}
                    type={type}
                    label={label}
                    error={Boolean(errors[fieldName])}
                    helperText={errors[fieldName]?.message}
                  />
                )}
                name={fieldName}
                control={control}
                rules={{ required }}
              />
            ),
          )}
        </DialogContent>
        <DialogActions>
          <Button onClick={onClose} color="inherit">
            Cancel
          </Button>
          <Button onClick={onSubmit} variant="contained">
            {submitActionText}
          </Button>
        </DialogActions>
      </Dialog>
    ),
  }
}
