import React from 'react'

export default function useReactPath() {
  const [path, setPath] = React.useState(
    process.browser && window.location.pathname,
  )
  const listenToPopstate = () => {
    const winPath = window.location.pathname
    setPath(winPath)
  }
  React.useEffect(() => {
    window.addEventListener('popstate', listenToPopstate)
    return () => {
      window.removeEventListener('popstate', listenToPopstate)
    }
  }, [])
  return path
}
