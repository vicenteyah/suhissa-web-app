import { SelectChangeEvent } from '@mui/material'
import { Client } from './entities/client'

export type ThemeMode = 'light' | 'dark'
export type ThemeDirection = 'rtl' | 'ltr'
export type ThemeColor =
  | 'default'
  | 'purple'
  | 'cyan'
  | 'blue'
  | 'orange'
  | 'red'

export type SettingsContextProps = {
  wellSelected: string
  clientSelected: number
  themeMode: ThemeMode
  themeDirection: ThemeDirection
  themeColor: ThemeColor
  themeStretch: boolean
  setColor: {
    name: string
    lighter: string
    light: string
    main: string
    dark: string
    darker: string
    contrastText: string
  }
  colorOption: {
    name: string
    value: string
  }[]
  onSelectWell: (wellName: string) => void
  onSelectClient: (client: Client) => void
  onChangeMode: (event: React.ChangeEvent<HTMLInputElement>) => void
  onChangeDirection: (event: React.ChangeEvent<HTMLInputElement>) => void
  onChangeColor: (event: React.ChangeEvent<HTMLInputElement>) => void
  onToggleStretch: VoidFunction
}
