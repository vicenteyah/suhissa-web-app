export type Url = string

export type TRecordId = number

type TConsesionAttributes = {
  translate: string
  title: string
  name: string
  volume: number
  rfc: string
  annexed: string
  maximumDischargeDiameter: number
}

type TPummpingInfrastructureAttributes = {
  translate: string
  title: string
  power: number
  pumpType: string
  pumpVoltage: number
  pipeMaterial: string
  pipeDiameter: number
}

type TAccumulatedVolumeAttributes = {
  translate: string
  title: string
  brand: string
  model: string
  id: string
  nsm: string
  nsut: string
  nsue: string
}

type TConductivityAttributes = {
  translate: string
  title: string
  brand: string
  model: string
  id: string
  nsm: string
  nsut: string
}

export type TWell = {
  id: TRecordId
  name: string
  place: string
  availabilty: number
  latitude: number
  longitude: number
  ce: number
  sdt: number
  dailyFlow: number
  consesion: TConsesionAttributes
  pumpingInfrastructure: TPummpingInfrastructureAttributes
  accumulatedVolume: TAccumulatedVolumeAttributes
  conductivity: TConductivityAttributes
}
