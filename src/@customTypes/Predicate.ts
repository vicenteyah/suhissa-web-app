type Predicate<Args extends any[]> = (...args: Args) => boolean

export default Predicate
