import { Session } from 'next-iron-session'
import SessionValue from './SessionValue'

type SimpleSession = Omit<Session, 'get' | 'set'>

interface GenericSession<T extends SessionValue> extends SimpleSession {
  get: (name: string) => T | undefined
  set: (name: string, value: T) => T
}

export default GenericSession
