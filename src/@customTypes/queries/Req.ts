import GenericSession from './GenericSession'
import SessionValue from './SessionValue'

interface Req<T extends SessionValue> {
  body: any
  session: GenericSession<T>
}

export default Req
