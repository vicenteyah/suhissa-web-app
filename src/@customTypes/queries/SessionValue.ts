import UserSession from '@customTypes/dto/UserSession'

// An union of all the types that `withSession` can use
type SessionValue = UserSession

export default SessionValue
