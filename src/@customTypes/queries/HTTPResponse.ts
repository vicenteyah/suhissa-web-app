interface HTTPResponse<Data> {
  status: number
  message: string
  data: Data
}

export default HTTPResponse
