import Req from './Req'
import SessionValue from './SessionValue'

interface TwoParamHandler<T extends SessionValue> {
  (req: Req<T>, res: any): any
}

interface OneParamHandler<T extends SessionValue> {
  (param: { req: Req<T>; res: any }): any
}

type Handler<T extends SessionValue> = OneParamHandler<T> | TwoParamHandler<T>

export type { Handler, OneParamHandler, TwoParamHandler }
