import { Client } from '@customTypes/entities/client'

interface IUserData {
  name: string
  username: string
  hasChangedPassword: boolean
  clients: Client[]
}

interface UserSession {
  isLoggedIn: boolean
  userData: IUserData
  token: string
}

export default UserSession
