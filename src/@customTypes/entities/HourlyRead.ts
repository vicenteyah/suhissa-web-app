import morphism, { StrictSchema } from 'morphism'

export type HourlyRead = {
  id: number
  readingDate: string
  consume: number
  volume: number
  caudal: number
  ce: number
  sdt: number
}

type HourlyReadDTO = {
  id: number
  readingDate: string
  volume: string
  read4: string
  ce: string
  sdt: string
}

const mapper = {
  id: 'id',
  readingDate: 'readingDate',
  consume: '',
  volume: 'volume',
  caudal: 'read4',
  ce: 'ce',
  sdt: 'sdt',
}

export function mutateHourlyRead(data: HourlyReadDTO[]) {
  return morphism<StrictSchema<HourlyRead, HourlyReadDTO>>(mapper, data)
}
