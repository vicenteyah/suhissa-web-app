export interface IUserLogin {
  password: string
  username: string
}
