import morphism, { StrictSchema } from 'morphism'

export type FlowmeterDetails = {
  rfc: string
  flowmeterImages: string[]
}

type FlowmeterDetailsDTO = {
  client: { rfc: string }
  flowmeterImages: string[]
}

const mapper = {
  rfc: 'client.rfc',
  flowmeterImages: 'flowmeterImages',
}

function mutateFlowmeterDetails(dto: FlowmeterDetailsDTO) {
  const details = morphism<StrictSchema<FlowmeterDetails, FlowmeterDetailsDTO>>(
    mapper,
    dto,
  )
  return details
}

export { mutateFlowmeterDetails }
