export type Flowmeter = {
  id: number
  name: string
  latitude: number
  longitude: number
  idNumber: string
  type: string
  operationType: string
  nsm: string
  concession: string
  concessionedVolume: string
  concessionedVolumeAttached: string
  attached: string
  unloadDiameter: string
  hp: string
  pumpType: string
  pumpVoltage: string
  pipeType: string
  pipeDiameter: string
  brand: string
  model: string
  nsut: string
  nsue: string
  brandm: string
  modelm: string
  rfc: string
  flowmeterImages: string[]
}

export type FlowmeterDTO = {
  id: number
  name: string
  latitude: string
  longitude: string
  idNumber: string
  nsm: string
  type: string
  operationtype: string
  concession: string
  concessioned_volume: string
  concessionVolume: string
  attached: string
  unload_diameter: string
  hp: string
  pump_type: string
  pump_voltage: string
  pipe_type: string
  pipe_diameter: string
  brand: string
  model: string
  nsut: string
  nsue: string
  brandm: string
  modelm: string
}

const mapper = {
  id: 'id',
  name: 'name',
  latitude: 'latitude',
  longitude: 'longitude',
  idNumber: 'idNumber',
  type: 'type',
  operationType: 'operationtype',
  nsm: 'nsm',
  concession: 'concession',
  concessionedVolume: 'concessioned_volume',
  concessionedVolumeAttached: 'concessionVolume',
  attached: 'attached',
  unloadDiameter: 'unload_diameter',
  hp: 'hp',
  pumpType: 'pump_type',
  pumpVoltage: 'pump_voltage',
  pipeType: 'pipe_type',
  pipeDiameter: 'pipe_diameter',
  brand: 'brand',
  model: 'model',
  nsut: 'nsut',
  nsue: 'nsue',
  brandm: 'brandm',
  modelm: 'modelm',
  rfc: '',
  flowmeterImages: '',
}

export type FlowmeterType = 'NMX' | 'SIMTCA' | undefined
export type FlowmeterLabel = {
  title: string
  value: string | number | string[]
}

type FlowmeterTitles = { title: string; prop: keyof Flowmeter }

type Fields = {
  consesion: FlowmeterTitles[]
  pumpingInfrastructure: FlowmeterTitles[]
  accumulatedVolume: FlowmeterTitles[]
  conductivity: FlowmeterTitles[]
}

export type FieldKeys = keyof Fields

const consesion: FlowmeterTitles[] = [
  { title: 'Título de concensión', prop: 'concession' },
  { title: 'RFC del concesionario', prop: 'rfc' },
  { title: 'Volumen concesionado', prop: 'concessionedVolume' },
  { title: 'Anexo', prop: 'attached' },
  {
    title: 'Diámetro de descarga (de la bomba)',
    prop: 'unloadDiameter',
  },
]

const pumpingInfrastructure: FlowmeterTitles[] = [
  { title: 'Potencia (HP)', prop: 'hp' },
  { title: 'Tipo de bomba', prop: 'pumpType' },
  { title: 'Voltaje de la bomba', prop: 'pumpVoltage' },
  { title: 'Material de la tubería', prop: 'pipeType' },
  { title: 'Diámetro de la tubería', prop: 'pipeDiameter' },
]

const accumulatedVolume: FlowmeterTitles[] = [
  { title: 'Marca', prop: 'brand' },
  { title: 'Modelo', prop: 'model' },
  { title: 'ID', prop: 'idNumber' },
  { title: '(NSM) No. De serie de medidor', prop: 'nsm' },
  { title: '(NSUT) No. De serie de la unidad de transmisión', prop: 'nsut' },
  { title: '(NSUE) No. de serie de la unidad electrónica', prop: 'nsue' },
]

const conductivity: FlowmeterTitles[] = [
  { title: 'Marca', prop: 'brand' },
  { title: 'Modelo', prop: 'model' },
  { title: 'ID', prop: 'idNumber' },
  { title: '(NSM) No. De serie de medidor', prop: 'nsm' },
  { title: '(NSUT) No. De serie de la unidad de transmisión', prop: 'nsut' },
]

const fields: Fields = {
  consesion,
  pumpingInfrastructure,
  accumulatedVolume,
  conductivity,
}

export { mapper, fields }
