import { morphism, StrictSchema } from 'morphism'

export type Client = {
  id: number
  name: string
}

type DTOClient = {
  id: number
  commercialName: string
}

const mapper = {
  id: 'id',
  name: 'commercialName',
}

export function mutateClient(objectSerialized: string) {
  const deserialized: DTOClient = JSON.parse(objectSerialized)
  return morphism<StrictSchema<Client, DTOClient>>(mapper, deserialized)
}

export function mutateClients(arraySerialized: string) {
  const deserialized: DTOClient[] = JSON.parse(arraySerialized)
  const clients = morphism<StrictSchema<Client, DTOClient>>(
    mapper,
    deserialized,
  )

  return clients
}
