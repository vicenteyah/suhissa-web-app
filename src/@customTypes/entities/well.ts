import { morphism, StrictSchema } from 'morphism'
import {
  FieldKeys,
  Flowmeter,
  FlowmeterDTO,
  FlowmeterLabel,
  mapper,
  fields,
  FlowmeterType,
} from './Flowmeter'

export type WellOld = {
  id: number
  name: string
  place: string
  availabilty: number
  latitude: number
  longitude: number
  ce: number
  sdt: number
  dailyFlow: number
  consesion: {
    translate: string
    title: string
    name: string
    volume: number
    rfc: string
    annexed: string
    maximumDischargeDiameter: number
  }
  pumpingInfrastructure: {
    translate: string
    title: string
    power: number
    pumpType: string
    pumpVoltage: number
    pipeMaterial: string
    pipeDiameter: number
  }
  accumulatedVolume: {
    translate: string
    title: string
    brand: string
    model: string
    id: string
    nsm: string
    nsut: string
    nsue: string
  }
  conductivity: {
    translate: string
    title: string
    brand: string
    model: string
    id: string
    nsm: string
    nsut: string
  }
}
export type WellPerDay = {
  id: number
  flowmeterId: number
  ceId: number
  ceReadId: number
  volumeId: number
  volumeReadId: number
  ce: string
  sdt: string
  volume: string
  date: string
}

export type Well = {
  NMX: Flowmeter
  SIMTCA: Flowmeter
}

export type Wells = {
  [key: string]: Well
}

function mutateWells(dtos: FlowmeterDTO[]) {
  const flowmeters = morphism<StrictSchema<Flowmeter, FlowmeterDTO>>(
    mapper,
    dtos,
  )
  let wells: Wells = {}

  flowmeters.forEach((flowmeter) => {
    wells = {
      ...wells,
      [flowmeter.name]: {
        ...(wells[flowmeter.name] || []),
        [flowmeter.operationType]: flowmeter,
      },
    }
  }, {})

  return wells
}

function mutateFlowmeter(dto: FlowmeterDTO) {
  return morphism<StrictSchema<Flowmeter, FlowmeterDTO>>(mapper, dto)
}

function getFlowmeter(well: Well, flometerType: FlowmeterType = undefined) {
  let flowmeter: Flowmeter
  if (flometerType) flowmeter = well[flometerType]
  else flowmeter = well.NMX || well.SIMTCA
  return flowmeter
}

function findWell(wells: Wells, id: number) {
  const names = Object.keys(wells)
  const matched = names.find((name) => getFlowmeter(wells[name]).id === id)
  return matched ? wells[matched] : undefined
}

function getLabels(fieldKey: FieldKeys, well: Well) {
  let labels: FlowmeterLabel[] = []
  const field = fields[fieldKey]
  labels = field.map((item) => {
    let label: FlowmeterLabel = {
      title: item.title,
      value: 'No Disponible',
    }

    const volumeFlowmeter = getFlowmeter(well, 'NMX')
    const ceFlowmeter = getFlowmeter(well, 'SIMTCA')
    if (volumeFlowmeter && fieldKey !== 'conductivity')
      label = {
        title: item.title,
        value: volumeFlowmeter[item.prop] || 'No Disponible',
      }
    if (ceFlowmeter && !['accumulatedVolume', 'consesion'].includes(fieldKey))
      label = {
        title: item.title,
        value: ceFlowmeter[item.prop] || 'No Disponible',
      }
    return label
  })

  return labels
}

export type ConsumptionPerDay = {
  id: number
  consumption: number
  date: string
}

export type DTOConsumptionPerDay = {
  id: number
  flowmeterId: number
  filename: string
  rfc: string
  nsm: string
  nsd: string
  ce: string | null
  parameters: string | null
  file: string
  ker: number
  sdt: string | null
  volume: string
  latitude: number
  longitude: number
  date: string | Date
  status: string
  hourlyLogStatus: string
  conaguaStatus: string
  createdAt: string | Date | null
  updatedAt: string | Date | null
  consumption: number
}

const ConsumptionMapper = {
  id: 'id',
  consumption: 'consumption',
  date: 'date',
}

export function mutateConsumptionPerDay(data: DTOConsumptionPerDay[]) {
  return morphism<StrictSchema<ConsumptionPerDay, DTOConsumptionPerDay>>(
    ConsumptionMapper,
    data,
  )
}

export { getLabels, mutateWells, mutateFlowmeter, getFlowmeter, findWell }
