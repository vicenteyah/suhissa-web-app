import { mutateHourlyRead } from '@customTypes/entities/HourlyRead'
import axios from 'axios'

async function getWellDetailsByHour(
  flowmeterId: number,
  userToken: string,
  year: number,
  month: number,
  day: number,
) {
  const url = process.env.NEXT_PUBLIC_IP_API_URL
  const wellDetails = await axios(
    `${url}flowmeters/${flowmeterId}/logs?year=${year}&month=${month}&day=${day}`,
    { headers: { authorization: userToken } },
  )
  return mutateHourlyRead(wellDetails.data.rows)
}

export { getWellDetailsByHour }
