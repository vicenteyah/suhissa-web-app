export default function generateDownloadUrl(
  clientSelected: number,
  userToken: string,
  flowmeterId: number,
  conductivityId: number,
) {
  return `${process.env.NEXT_PUBLIC_API_URL}clients/${clientSelected}/flowmeters/${flowmeterId}/conductivities/${conductivityId}/download?token=${userToken}`
}