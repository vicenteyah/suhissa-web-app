import { mutateClients } from '@customTypes/entities/client'
import axios from 'axios'

async function getClients(idUser: number, userToken: string) {
  const url = process.env.NEXT_PUBLIC_API_URL

  const clients = await axios(`${url}users/${idUser}`, {
    headers: { authorization: userToken },
  })
  return mutateClients(JSON.stringify(clients))
}

export { getClients }
