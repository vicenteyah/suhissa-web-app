import axios from 'axios'

async function getAverageVolume(clientId: number) {
  const url = process.env.NEXT_PUBLIC_DASHBOARD_API_URL
  const response = await axios(`${url}dailyavg/${clientId}`)
  return response.data.avg
}

export { getAverageVolume }
