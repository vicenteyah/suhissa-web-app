import { mutateFlowmeterDetails } from '@customTypes/entities/FlowmeterDetails'
import { mutateFlowmeter } from '@customTypes/entities/well'
import axios from 'axios'

async function getDetails(flowmeterId: number, userToken: string) {
  const url = process.env.NEXT_PUBLIC_API_URL
  const flowmeterDetails = await axios(`${url}flowmeters/${flowmeterId}`, {
    headers: { authorization: userToken },
  })
  return flowmeterDetails
}

async function getFlowmeterDetails(flowmeterId: number, userToken: string) {
  const flowmeterDetails = await getDetails(flowmeterId, userToken)
  return mutateFlowmeterDetails(flowmeterDetails.data.result)
}

async function getAllFlowmeterDetails(flowmeterId: number, userToken: string) {
  const flowmeterDetails = await getDetails(flowmeterId, userToken)
  return mutateFlowmeter(flowmeterDetails.data.result)
}

export { getFlowmeterDetails, getAllFlowmeterDetails }
