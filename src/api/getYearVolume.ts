import axios from 'axios'

async function getYearVolume(
  flowmeterId: number,
  userToken: string,
  year: number,
) {
  const url = process.env.NEXT_PUBLIC_IP_API_URL
  const wellDetails = await axios(
    `${url}flowmeters/${flowmeterId}/conductivities/yearVolume?startYear=${year}`,
    { headers: { authorization: userToken } },
  )
  return wellDetails.data.yearVolume
}

export { getYearVolume }
