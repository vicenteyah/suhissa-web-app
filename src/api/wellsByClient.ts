import { mutateWells } from '@customTypes/entities/well'
import axios from 'axios'

async function getWellsByClient(idClient: number, userToken: string) {
  try {
    const url = process.env.NEXT_PUBLIC_API_URL

    const wells = await axios(`${url}clients/${idClient}/flowmeters`, {
      headers: { authorization: userToken },
    })

    return mutateWells(wells.data.rows)
  } catch (error: any) {
    return error.response.status
  }
}

async function getDailyRead(
  flowmeterId: number,
  userToken: string,
  year: number,
  month: number,
) {
  const url = process.env.NEXT_PUBLIC_IP_API_URL
  const well = await axios(
    `${url}flowmeters/${flowmeterId}/conductivities?year=${year}&month=${month}`,
    { headers: { authorization: userToken } },
  )

  return well.data.rows
}

export { getWellsByClient, getDailyRead }
