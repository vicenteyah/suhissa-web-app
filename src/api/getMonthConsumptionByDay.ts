import axios from 'axios'
import { mutateConsumptionPerDay } from '@customTypes/entities/well'

async function getMonthConsumptionByDay(
  idClient: number,
  flowmeterId: number,
  userToken: string,
  year: number,
  month: number,
) {
  const url = process.env.NEXT_PUBLIC_API_URL
  const consumptionByDay = await axios(
    `${url}clients/${idClient}/flowmeters/${flowmeterId}/conductivities/monthConsumptionByDay?year=${year}&month=${month}`,
    { headers: { authorization: userToken } },
  )
  return mutateConsumptionPerDay(consumptionByDay.data.rows)
}

export { getMonthConsumptionByDay }
