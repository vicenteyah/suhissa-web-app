import { ReactNode, createContext } from 'react'
import SetColor from 'theme/colors'
import { Client } from '@customTypes/entities/client'
import useLocalStorage from '../hooks/useLocalStorage'
import {
  ThemeMode,
  ThemeDirection,
  ThemeColor,
  SettingsContextProps,
} from '../@customTypes/settings'

const PRIMARY_COLOR = SetColor('blue')

const initialState: SettingsContextProps = {
  wellSelected: '',
  clientSelected: 0,
  themeMode: 'light',
  themeDirection: 'ltr',
  themeColor: 'default',
  themeStretch: false,
  onSelectWell: () => {},
  onSelectClient: () => {},
  onChangeMode: () => {},
  onChangeDirection: () => {},
  onChangeColor: () => {},
  onToggleStretch: () => {},
  setColor: PRIMARY_COLOR,
  colorOption: [],
}

const SettingsContext = createContext(initialState)

type SettingsProviderProps = {
  children: ReactNode
}

function SettingsProvider({ children }: SettingsProviderProps) {
  const [settings, setSettings] = useLocalStorage('settings', {
    wellSelected: '',
    clientSelected: 0,
    themeMode: 'light',
    themeDirection: 'ltr',
    themeColor: 'default',
    themeStretch: false,
  })

  const onSelectWell = (wellName: string) => {
    setSettings({
      ...settings,
      wellSelected: wellName,
    })
  }

  const onSelectClient = (client: Client) => {
    setSettings({
      ...settings,
      clientSelected: client.id,
    })
  }

  const onChangeMode = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSettings({
      ...settings,
      themeMode: (event.target as HTMLInputElement).value as ThemeMode,
    })
  }

  const onChangeDirection = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSettings({
      ...settings,
      themeDirection: (event.target as HTMLInputElement)
        .value as ThemeDirection,
    })
  }

  const onChangeColor = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSettings({
      ...settings,
      themeColor: (event.target as HTMLInputElement).value as ThemeColor,
    })
  }

  const onToggleStretch = () => {
    setSettings({
      ...settings,
      themeStretch: !settings.themeStretch,
    })
  }

  return (
    <SettingsContext.Provider
      value={{
        ...settings,
        // well Selected
        onSelectWell,
        // client Selected
        onSelectClient,
        // Mode
        onChangeMode,
        // Direction
        onChangeDirection,
        // Color
        onChangeColor,
        setColor: PRIMARY_COLOR,
        colorOption: {
          name: PRIMARY_COLOR.name,
          value: PRIMARY_COLOR.main,
        },
        // Stretch
        onToggleStretch,
      }}
    >
      {children}
    </SettingsContext.Provider>
  )
}

export { SettingsProvider, SettingsContext }
