const getProperty =
  <T extends Record<string, any>>(key: string) =>
  (obj: T) =>
    obj[key] ?? obj

export default getProperty
