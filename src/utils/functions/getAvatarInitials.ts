export function getAvatarInitials(name: string) {
  const words = name.split(' ')
  const initials = words.map((word) => word.substring(0, 1))
  const newWord = initials.join('').toUpperCase()
  return newWord.substring(0, 2)
}
