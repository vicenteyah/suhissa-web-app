export default function getLastItem(thePath: string) {
  return thePath.substring(thePath.lastIndexOf('/') + 1)
}
