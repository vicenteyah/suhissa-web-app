const numberFormat = new Intl.NumberFormat('es-MX')

export default function formatNumber(number: number) {
  return numberFormat.format(number)
}
