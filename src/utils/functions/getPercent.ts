export default function getPercent(value: number, total: number) {
  let percent = 0
  if (value >= 0) {
    percent = Number(((value * 100) / total).toFixed(2))
  }
  return percent
}
