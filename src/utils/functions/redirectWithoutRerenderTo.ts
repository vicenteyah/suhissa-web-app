export default function redirectWithoutRerenderTo(urlName: string) {
  return window.history.replaceState(
    { ...window.history.state, as: urlName, url: urlName },
    '',
    urlName,
  )
}
