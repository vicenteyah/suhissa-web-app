import Predicate from '@customTypes/Predicate'

type Not = <Args extends any[]>(predicate: Predicate<Args>) => Predicate<Args>

const not: Not =
  (predicate) =>
  (...args) =>
    !predicate(...args)

export default not
