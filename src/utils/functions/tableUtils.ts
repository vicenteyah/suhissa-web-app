import { IBrand } from '@customTypes/entities/brand'
import { filter } from 'lodash'

type Anonymous = Record<string | number, string>

function descendingComparator(a: Anonymous, b: Anonymous, orderBy: string) {
  if (b[orderBy] < a[orderBy]) {
    return -1
  }
  if (b[orderBy] > a[orderBy]) {
    return 1
  }
  return 0
}

function getComparator(order: string, orderBy: string) {
  return order === 'desc'
    ? (a: Anonymous, b: Anonymous) => descendingComparator(a, b, orderBy)
    : (a: Anonymous, b: Anonymous) => -descendingComparator(a, b, orderBy)
}

function applySortFilter(
  array: IBrand[],
  comparator: (a: any, b: any) => number,
  query: string,
) {
  const stabilizedThis = array.map((el, index) => [el, index] as const)
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0])
    if (order !== 0) return order
    return a[1] - b[1]
  })
  if (query) {
    return filter(
      array,
      (brand) => brand.name.toLowerCase().indexOf(query.toLowerCase()) !== -1,
    )
  }
  return stabilizedThis.map((el) => el[0])
}

export type { Anonymous }
export { descendingComparator, getComparator, applySortFilter }
