import { ITab } from 'store/tabStore'

export default function findTab(tabs: ITab[], pathname: string) {
  return tabs.find((element) => pathname.includes(element.name))
}
