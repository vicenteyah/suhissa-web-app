import React from 'react'
import Link from 'next/link'
import Tooltip from '@mui/material/Tooltip'
import Opacity from '@mui/icons-material/Opacity'
import Visibility from '@mui/icons-material/Visibility'
import { Box, IconButton } from '@mui/material'

type LinkReportProps = {
  ceReport: string | 0
  volumeReport: string | 0
  detailwells: string
}

export default function LinkReport({
  ceReport,
  volumeReport,
  detailwells,
}: LinkReportProps) {
  return (
    <Box sx={{ display: 'flex', alignItems: 'center', gap: '10px' }}>
      {ceReport ? (
        <Tooltip title="Lectura SITMCA" sx={{ mt: 2 }}>
          <IconButton href={ceReport}>
            <Opacity color="info" />
          </IconButton>
        </Tooltip>
      ) : (
        <Tooltip title="Lectura SITMCA" sx={{ mt: 2 }}>
          <Opacity color="disabled" />
        </Tooltip>
      )}

      {volumeReport ? (
        <Tooltip title="Lectura SITMCO" sx={{ mt: 2 }}>
          <IconButton href={volumeReport}>
            <Opacity color="primary" />
          </IconButton>
        </Tooltip>
      ) : (
        <Tooltip title="Lectura SITMCO" sx={{ mt: 2 }}>
          <Opacity color="disabled" />
        </Tooltip>
      )}
      <Link href={detailwells}>
        <a>
          <Tooltip title="Ver detalles" sx={{ mt: 2 }}>
            <IconButton>
              <Visibility color="action" />
            </IconButton>
          </Tooltip>
        </a>
      </Link>
    </Box>
  )
}
