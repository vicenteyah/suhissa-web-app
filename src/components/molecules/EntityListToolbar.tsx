import { Icon } from '@iconify/react'
import searchFill from '@iconify/icons-eva/search-fill'
import trash2Fill from '@iconify/icons-eva/trash-2-fill'
import roundFilterList from '@iconify/icons-ic/round-filter-list'
import { useTheme } from '@material-ui/core/styles'
import {
  Box,
  Tooltip,
  IconButton,
  Typography,
  InputAdornment,
} from '@material-ui/core'
import { RootStyle, SearchStyle } from '../atoms/EntityListToolbar'

type EntityListToolbarProps = {
  numSelected: number
  filterName: string
  onFilterName: (value: string) => void
  entity: string
  onDeleteAll: VoidFunction
}

export default function EntityListToolbar({
  numSelected,
  filterName,
  onFilterName,
  entity,
  onDeleteAll,
}: EntityListToolbarProps) {
  const theme = useTheme()
  const isLight = theme.palette.mode === 'light'

  return (
    <RootStyle
      sx={{
        ...(numSelected > 0 && {
          color: isLight ? 'primary.main' : 'text.primary',
          bgcolor: isLight ? 'primary.lighter' : 'primary.dark',
        }),
      }}
    >
      {numSelected > 0 ? (
        <Typography component="div" variant="subtitle1">
          {numSelected} selected
        </Typography>
      ) : (
        <SearchStyle
          value={filterName}
          onChange={(e) => onFilterName(e.target.value)}
          placeholder={`Buscar ${entity}...`}
          startAdornment={
            <InputAdornment position="start">
              <Box
                component={Icon}
                icon={searchFill}
                sx={{ color: 'text.disabled' }}
              />
            </InputAdornment>
          }
        />
      )}

      {numSelected > 0 ? (
        <Tooltip title="Delete">
          <IconButton>
            <Icon onClick={onDeleteAll} icon={trash2Fill} />
          </IconButton>
        </Tooltip>
      ) : (
        <Tooltip title="Filter list">
          <IconButton>
            <Icon icon={roundFilterList} />
          </IconButton>
        </Tooltip>
      )}
    </RootStyle>
  )
}
