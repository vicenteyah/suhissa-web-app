import { Paper, PaperProps, Typography } from '@material-ui/core'

interface SearchNotFoundProps extends PaperProps {
  searchQuery?: string
}

export default function SearchNotFound({
  searchQuery = '',
  ...other
}: SearchNotFoundProps) {
  return (
    <Paper {...other}>
      <Typography gutterBottom align="center" variant="subtitle1">
        No encontrado
      </Typography>
      <Typography variant="body2" align="center">
        No se encontraron resultados para &nbsp;
        <strong>&quot;{searchQuery}&quot;</strong>. Intenta revisar errores de
        redacción o utilizando palabras concretas.
      </Typography>
    </Paper>
  )
}
