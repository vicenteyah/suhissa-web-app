import { Client } from '@customTypes/entities/client'
import {
  Box,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  SelectChangeEvent,
} from '@mui/material'
import { SettingsContext } from 'contexts/SettingsContext'
import useLocalStorage from 'hooks/useLocalStorage'
import React, { useContext, useEffect, useState } from 'react'

export default function SelectClient() {
  const { onSelectClient, clientSelected } = useContext(SettingsContext)
  const [path, setPath] = useState('')

  useEffect(() => {
    setPath(window.location.pathname)
  }, [path])

  const [clients, setClients] = useLocalStorage('clients', [])

  const handleSelect = (event: SelectChangeEvent) => {
    const selected = clients.find(
      (client: Client) => client.id === parseInt(event.target.value, 10),
    )
    onSelectClient(selected || { id: 0, name: '' })
  }

  return (
    <Box sx={{ minWidth: 120 }}>
      <FormControl fullWidth disabled={path === '/dashboard/detailwells'}>
        <InputLabel id="demo-simple-select-label">Cliente</InputLabel>
        <Select
          size="small"
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={`${clientSelected}`}
          label="Cliente"
          onChange={handleSelect}
        >
          {clients.map((client: Client) => (
            <MenuItem key={client.id} value={client.id}>
              {client.name}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Box>
  )
}
