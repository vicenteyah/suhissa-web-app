import {
  Badge,
  Card,
  Chip,
  Grid,
  Tooltip,
  Typography,
  useTheme,
} from '@mui/material'
import React from 'react'
import formatNumber from 'utils/functions/formatNumber'
import dynamic from 'next/dynamic'
import getPercent from 'utils/functions/getPercent'
import { ApexOptions } from 'apexcharts'
import { Help } from '@mui/icons-material'

const ReactApexChart = dynamic(() => import('react-apexcharts'), { ssr: false })

type DataBoxProps = {
  title: string
  value: number
  dataUnity: string
  total?: number
  helpText?: string
  hideChart?: boolean
}
export default function DataBox({
  title,
  value,
  dataUnity,
  total = undefined,
  helpText = undefined,
  hideChart = false,
}: DataBoxProps) {
  const theme = useTheme()

  const series = [value, (total && total - value) || 0]

  const options: ApexOptions = {
    chart: {
      type: 'donut',
    },
    legend: {
      show: false,
    },
    dataLabels: {
      enabled: false,
    },
    tooltip: {
      enabled: false,
    },
    colors: [
      value <= (total || value)
        ? theme.palette.primary.dark
        : theme.palette.error.dark,
      theme.palette.grey['300'],
    ],
  }

  return (
    <Card sx={{ p: 2 }}>
      <Typography
        variant="subtitle2"
        color={theme.palette.grey[600]}
        paragraph
        sx={{ display: 'flex', alignItems: 'center' }}
      >
        {title}
        {helpText && (
          <Tooltip title={helpText}>
            <Help
              sx={{
                fontSize: 16,
                ml: 1,
                color: (theme) => theme.palette.grey[500],
              }}
            />
          </Tooltip>
        )}
      </Typography>

      <Grid container>
        <Grid item xs={9}>
          <Typography variant="h4" paragraph sx={{ mb: 0.5 }}>
            {value > 0 ? formatNumber(value) : 0}
            <Typography variant="caption" sx={{ ml: 0.5 }} fontWeight="bold">
              {dataUnity}
            </Typography>
          </Typography>

          <Typography
            color={theme.palette.grey[100]}
            fontWeight="bold"
            borderRadius="20px"
            padding="2px 15px"
            textAlign="center"
            variant="caption"
            sx={{
              backgroundColor:
                value <= (total || value)
                  ? theme.palette.primary.dark
                  : theme.palette.error.dark,
              display: 'inline-block',
            }}
          >
            {total && `${formatNumber(getPercent(value, total))}%`}
          </Typography>
        </Grid>
        <Grid item xs={3}>
          {total && !hideChart && (
            <ReactApexChart
              type="donut"
              options={options}
              series={series}
              height="90px"
            />
          )}
        </Grid>
      </Grid>
    </Card>
  )
}
