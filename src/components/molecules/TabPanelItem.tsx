import React, { useEffect } from 'react'
import TabPanel from '@mui/lab/TabPanel'
import AccordionsWell from 'components/layouts/wells/AccordionsWell'
import Details from 'components/layouts/wells/Details'
import BarChart from 'components/layouts/wells/BarChart'
import DownloadReports from 'components/layouts/wells/DownloadReports'
import SortingSelecting from 'components/layouts/wells/table/index'
import {
  ConsumptionPerDay,
  getFlowmeter,
  Well,
  WellOld,
  WellPerDay,
} from '@customTypes/entities/well'
import { SettingsContext } from 'contexts/SettingsContext'
import { getDailyRead } from 'api/wellsByClient'
import { getMonthConsumptionByDay } from 'api/getMonthConsumptionByDay'

import { Typography, Grid, Card } from '@mui/material'
import moment from 'moment'
import SkeletonTabContent from 'components/layouts/skeletons/TabContent'
import SkeletonBarChart from 'components/layouts/skeletons/BarChart'

import { getYearVolume } from 'api/getYearVolume'
import { getWellDetailsByHour } from 'api/getWellDetailsByHour'
import { HourlyRead } from '@customTypes/entities/HourlyRead'
import { If, Then } from 'react-if'
import useLocalStorage from 'hooks/useLocalStorage'

interface TabPanelItemProps {
  currentWell: Well
  sx?: object
  setLoadingContent: React.Dispatch<React.SetStateAction<boolean>>
  loadingContent: boolean
}

function PanelItem({
  currentWell,
  sx,
  setLoadingContent,
  loadingContent,
}: TabPanelItemProps) {
  const { clientSelected } = React.useContext(SettingsContext)
  const [userToken, setUserToken] = useLocalStorage('token', '')
  const [wellsPerDay, setWellsPerDay] = React.useState<WellPerDay[]>()
  const [conductivity, setConductivity] = React.useState(0)
  const [yearVolume, setYearVolume] = React.useState(0)
  const [lastUpdate, setLastUpdate] = React.useState('')
  const [lastRead, setLastRead] = React.useState(0)
  const [caudal, setCaudal] = React.useState(0)

  const [sdt, setSdt] = React.useState(0)
  const [wellsPerDayChanged, setWellsPerDayChanged] =
    React.useState<boolean>(false)
  const [monthConsumptionByDay, setMonthConsumptionByDay] = React.useState<
    ConsumptionPerDay[]
  >([])
  const [consumptionByDayDataLoading, setConsumptionByDayDataLoading] =
    React.useState<boolean>(false)

  const getDailyReads = async (year: number, month: number) => {
    setWellsPerDayChanged(false)
    let ceData: WellPerDay[] = []
    let volumeData: WellPerDay[] = []
    const dailyReads = []
    if (userToken) {
      if (currentWell.SIMTCA) {
        ceData = await getDailyRead(
          currentWell.SIMTCA.id,
          userToken,
          year,
          month,
        )
      }
      if (currentWell.NMX) {
        volumeData = await getDailyRead(
          currentWell.NMX.id,
          userToken,
          year,
          month,
        )
      }

      const date = moment(`${year}-${month}-01`).add(1, 'M').subtract(1, 'd')
      for (let i = 0; i <= date.daysInMonth(); i += 1) {
        const ceMatched = ceData.find(
          (ceRead) => ceRead.date.split('T')[0] === date.format('YYYY-MM-DD'),
        )

        const volumeMatched = volumeData.find(
          (volRead) => volRead.date.split('T')[0] === date.format('YYYY-MM-DD'),
        )
        const dailyRead: WellPerDay = {
          id: ceMatched?.id || volumeMatched?.id || 0,
          flowmeterId:
            ceMatched?.flowmeterId || volumeMatched?.flowmeterId || 0,
          ceId: ceMatched?.flowmeterId || 0,
          ceReadId: ceMatched?.id || 0,
          volumeId: volumeMatched?.flowmeterId || 0,
          volumeReadId: volumeMatched?.id || 0,
          ce: ceMatched?.ce || '',
          sdt: ceMatched?.sdt || '',
          volume: volumeMatched?.volume || '',
          date: date.format('YYYY-MM-DD'),
        }

        if (dailyRead.id) dailyReads.push(dailyRead)
        date.subtract(1, 'd')
      }
      setWellsPerDay(dailyReads)
    }

    setWellsPerDayChanged(true)
    return dailyReads
  }

  const updateYearVolume = async (flowmeterId: number) => {
    const currentYear = moment().year()
    const newVolume = await getYearVolume(
      flowmeterId,
      String(userToken),
      currentYear,
    )
    return newVolume
  }

  const getMonthConsumptionByDayData = async (year: number, month: number) => {
    try {
      setConsumptionByDayDataLoading(true)
      setMonthConsumptionByDay([])
      if (currentWell.NMX) {
        const data: ConsumptionPerDay[] = await getMonthConsumptionByDay(
          clientSelected,
          currentWell.NMX.id,
          String(userToken),
          year,
          month,
        )
        if (data.length > 0 && data.some((row) => row.consumption > 0)) {
          setMonthConsumptionByDay(data)
        }
      }

      setConsumptionByDayDataLoading(false)
    } catch (error) {
      setConsumptionByDayDataLoading(false)
    }
  }

  const getTodayReads = async () => {
    const now = moment()
    const defaultFlowmeter = getFlowmeter(currentWell)

    const todayReads = await getWellDetailsByHour(
      defaultFlowmeter.id,
      String(userToken),
      now.year(),
      now.month() + 1,
      now.date(),
    )
    return todayReads
  }

  const getCaudalAvg = (reads: HourlyRead[]) => {
    let caudalReads = 0
    const sumCaudal = reads.reduce((sum, read) => {
      if (read.caudal > 0) caudalReads += 1
      return Number(sum) + Number(read.caudal)
    }, 0)

    return caudalReads ? sumCaudal / caudalReads : 0
  }

  const calculateIndicators = async () => {
    setConductivity(0)
    setSdt(0)
    setYearVolume(0)
    setLastUpdate('')
    setLastRead(0)
    setCaudal(0)

    const currentDate = moment().add(1, 'M')

    if (currentWell.NMX) {
      const yearVolume = await updateYearVolume(currentWell.NMX.id)
      if (typeof yearVolume === 'number' && yearVolume > 0)
        setYearVolume(yearVolume)
    }

    getMonthConsumptionByDayData(currentDate.year(), currentDate.month())

    const dailyReads = await getDailyReads(
      currentDate.year(),
      currentDate.month(),
    )

    const todayReads = await getTodayReads()
    if (todayReads.length > 0) {
      setLastUpdate(todayReads[0].readingDate)
      setLastRead(todayReads[0].volume || 0)
      if (currentWell.NMX) {
        setCaudal(getCaudalAvg(todayReads))
      }
    }

    setConductivity(Number(dailyReads.at(-1)?.ce))
    setSdt(Number(dailyReads.at(-1)?.sdt))
    setLoadingContent(false)
  }

  const handleDate = (date: Date | null) => {
    if (date) {
      getDailyReads(new Date(date).getFullYear(), new Date(date).getMonth() + 1)
      getMonthConsumptionByDayData(
        new Date(date).getFullYear(),
        new Date(date).getMonth() + 1,
      )
    }
  }

  useEffect(() => {
    if (userToken) {
      calculateIndicators()
    }
  }, [userToken, currentWell])

  return !loadingContent ? (
    <TabPanel value={String(getFlowmeter(currentWell).id)} sx={{ ...sx }}>
      <AccordionsWell well={currentWell} />
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <Details
            yearVolume={yearVolume}
            currentWell={currentWell}
            conductivity={conductivity}
            lastUpdate={lastUpdate}
            lastRead={lastRead}
            caudal={caudal}
            sdt={sdt}
            photos={getFlowmeter(currentWell).flowmeterImages}
          />
        </Grid>
      </Grid>
      <Card sx={{ p: 2, mt: 2 }}>
        <Typography sx={{ ml: 2, mr: 2 }} variant="subtitle2" paragraph>
          Consumo promedio diario
        </Typography>
        <If condition={consumptionByDayDataLoading}>
          <Then>
            <Typography
              variant="h3"
              sx={{
                textAlign: 'center',
              }}
            >
              <SkeletonBarChart />
            </Typography>
          </Then>
        </If>
        <If
          condition={
            monthConsumptionByDay.length > 0 && !consumptionByDayDataLoading
          }
        >
          <Then>
            <BarChart
              showYAxis={false}
              categories={monthConsumptionByDay.map((data) =>
                moment(data.date).format('DD MMM'),
              )}
              data={monthConsumptionByDay.map((data) =>
                data.consumption > 0 ? data.consumption : 0,
              )}
              title=""
            />
          </Then>
        </If>
        {/* <If
          condition={
            monthConsumptionByDay.length === 0 && !consumptionByDayDataLoading
          }
        >
        EMPTY STATE
        </If> */}

        <SortingSelecting
          well={wellsPerDay}
          handleDate={handleDate}
          wellsPerDayChanged={wellsPerDayChanged}
        />
        <DownloadReports
          userToken={userToken}
          volumeId={getFlowmeter(currentWell, 'NMX')?.id || 0}
          ceId={getFlowmeter(currentWell, 'SIMTCA')?.id || 0}
        />
      </Card>
    </TabPanel>
  ) : (
    <SkeletonTabContent />
  )
}

export default PanelItem
