import { Flowmeter } from '@customTypes/entities/Flowmeter'
import { Typography, useTheme } from '@mui/material'
import React from 'react'

type PopupWellLocationProps = {
  well: Flowmeter
}
export default function PopupWellLocation({ well }: PopupWellLocationProps) {
  const theme = useTheme()
  return (
    <div>
      <Typography variant="subtitle1" textAlign="center">
        <a
          style={{ color: theme.palette.primary.dark }}
          href={`/dashboard/wells/?well=${well.id}`}
        >
          {well.name}
        </a>
      </Typography>
      <p>
        <strong>Volumen concesionado: </strong>
        {well.concessionedVolume ? well.concessionedVolume : 'Sin información'}
      </p>
    </div>
  )
}
