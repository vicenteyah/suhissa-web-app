import { useState, useEffect } from 'react'
import { Icon } from '@iconify/react'
import arrowIosForwardFill from '@iconify/icons-eva/arrow-ios-forward-fill'
import arrowIosDownwardFill from '@iconify/icons-eva/arrow-ios-downward-fill'
import { useRouter } from 'next/router'
import { alpha, useTheme, styled } from '@material-ui/core/styles'
import {
  Box,
  List,
  Collapse,
  BoxProps,
  ListItemText,
  ListItemIcon,
  ListSubheader,
  ListItemButton,
  Button,
} from '@material-ui/core'
import useTabsStore, {
  useSelectedTabStore,
  ISelectedTabState,
} from 'store/tabStore'
import findTab from 'utils/functions/findTab'
import getLastItem from 'utils/functions/getLastItemFromPath'
import redirectWithoutRerenderTo from 'utils/functions/redirectWithoutRerenderTo'

const ListSubheaderStyle = styled((props) => (
  <ListSubheader disableSticky disableGutters {...props} />
))(({ theme }) => ({
  ...theme.typography.overline,
  marginTop: theme.spacing(3),
  marginBottom: theme.spacing(2),
  paddingLeft: theme.spacing(5),
  color: theme.palette.text.primary,
}))

const ListItemStyle = styled(ListItemButton)(({ theme }) => ({
  ...theme.typography.body2,
  height: 48,
  position: 'relative',
  textTransform: 'capitalize',
  paddingLeft: theme.spacing(5),
  paddingRight: theme.spacing(2.5),
  color: theme.palette.grey[300],
  '&:before': {
    top: 0,
    right: 0,
    width: 3,
    bottom: 0,
    content: "''",
    display: 'none',
    position: 'absolute',
    borderTopLeftRadius: 4,
    borderBottomLeftRadius: 4,
    backgroundColor: theme.palette.primary.main,
  },
}))

const ListItemIconStyle = styled(ListItemIcon)({
  width: 22,
  height: 22,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
})

type NavItemProps = {
  title: string
  path: string
  icon?: JSX.Element
  info?: JSX.Element
  children?: {
    title: string
    path: string
  }[]
}

function NavItem({
  item,
  isShow,
}: {
  item: NavItemProps
  isShow?: boolean | undefined
}) {
  const theme = useTheme()
  const { pathname, push } = useRouter()
  const { title, path, icon, info, children } = item
  const [isActiveRoot, setIsActiveRoot] = useState(pathname.includes(path))
  const [open, setOpen] = useState(isActiveRoot)
  const { selectedTab, setSelectedTab } = useSelectedTabStore()
  const { tabs }: any = useTabsStore()

  const unsubscribeFromStore = useSelectedTabStore.subscribe(
    ({ selectedTab }: ISelectedTabState) => {
      setIsActiveRoot(!!selectedTab?.componentPath.includes(path))
    },
  )

  useEffect(() => {
    return unsubscribeFromStore()
  }, [])

  const handleOpen = () => {
    setOpen(!open)
  }

  const onChangeItem = (href: string) => (e: React.SyntheticEvent) => {
    const foundTab = findTab(tabs, getLastItem(href))
    if (foundTab && foundTab.ContentComponent) {
      setSelectedTab(foundTab)
      redirectWithoutRerenderTo(foundTab.componentPath)
    } else {
      push(href)
    }
  }

  const activeRootStyle = {
    color: theme.palette.grey[100],
    fontWeight: 'fontWeightMedium',
    bgcolor: theme.palette.grey[50048],
    '&:before': { display: 'block' },
    width: '100%',
  }

  const buttonRootStyle = {
    width: '100%',
    margin: 0,
    padding: 0,
  }

  const activeSubStyle = {
    color: 'text.primary',
    fontWeight: 'fontWeightMedium',
  }

  if (children) {
    return (
      <>
        <ListItemStyle
          disableGutters
          onClick={handleOpen}
          sx={{
            ...(isActiveRoot && activeRootStyle),
          }}
        >
          <ListItemIconStyle>{icon && icon}</ListItemIconStyle>

          {isShow && (
            <>
              <ListItemText disableTypography primary={title} />
              {info && info}
              <Box
                component={Icon}
                icon={open ? arrowIosDownwardFill : arrowIosForwardFill}
                sx={{ width: 16, height: 16, ml: 1 }}
              />
            </>
          )}
        </ListItemStyle>

        {isShow && (
          <Collapse in={open} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              {children.map((item) => {
                const { title, path } = item
                const isActiveSub = pathname.includes(path)

                return (
                  <Button
                    key={title}
                    onClick={onChangeItem(path)}
                    sx={{ ...buttonRootStyle }}
                  >
                    <ListItemStyle
                      disableGutters
                      sx={{
                        ...(isActiveSub && activeSubStyle),
                      }}
                    >
                      <ListItemIconStyle>
                        <Box
                          component="span"
                          sx={{
                            width: 4,
                            height: 4,
                            display: 'flex',
                            borderRadius: '50%',
                            alignItems: 'center',
                            justifyContent: 'center',
                            bgcolor: 'text.disabled',
                            transition: (theme) =>
                              theme.transitions.create('transform'),
                            ...(isActiveSub && {
                              transform: 'scale(2)',
                              bgcolor: 'primary.main',
                            }),
                          }}
                        />
                      </ListItemIconStyle>
                      <ListItemText disableTypography primary={title} />
                    </ListItemStyle>
                  </Button>
                )
              })}
            </List>
          </Collapse>
        )}
      </>
    )
  }

  return (
    <Button sx={{ ...buttonRootStyle }} onClick={onChangeItem(path)}>
      <ListItemStyle
        disableGutters
        sx={{
          ...(isActiveRoot && activeRootStyle),
        }}
      >
        <ListItemIconStyle>{icon && icon}</ListItemIconStyle>
        {isShow && (
          <>
            <ListItemText disableTypography primary={item.title} />
            {info && info}
          </>
        )}
      </ListItemStyle>
    </Button>
  )
}

interface NavSectionProps extends BoxProps {
  isShow?: boolean | undefined
  navConfig: {
    subheader: string
    items: NavItemProps[]
  }[]
}

export default function NavSection({
  navConfig,
  isShow = true,
  ...other
}: NavSectionProps) {
  return (
    <Box {...other}>
      {navConfig.map((list) => {
        const { subheader, items } = list
        return (
          <List key={subheader} disablePadding>
            {isShow && (
              <ListSubheaderStyle
                sx={{ color: (theme) => theme.palette.grey[200] }}
              >
                {subheader}
              </ListSubheaderStyle>
            )}
            {items.map((item: NavItemProps) => (
              <NavItem key={item.title} item={item} isShow={isShow} />
            ))}
          </List>
        )
      })}
    </Box>
  )
}
