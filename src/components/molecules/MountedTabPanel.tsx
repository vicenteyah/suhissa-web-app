import { useTabContext } from '@mui/lab'

export default function TabPanel(props: any) {
  const { children, className, style, value: id, ...other } = props
  const context = useTabContext()

  if (context === null) {
    throw new TypeError('No TabContext provided')
  }
  const tabId = context.value

  return (
    <div
      className={className}
      style={{
        width: '100%',
        ...style,
        position: 'absolute',
        left: 0,
        visibility: id === tabId ? 'visible' : 'hidden',
      }}
      {...other}
    >
      {children}
    </div>
  )
}
