import { Help } from '@mui/icons-material'
import { Card, Grid, Tooltip, Typography, useTheme } from '@mui/material'
import { ReactNode } from 'react'

export default function DataBoxText({
  title,
  children,
  textColor = 'black',
  helpText = undefined,
}: {
  title: string
  children: ReactNode
  textColor?: string
  helpText?: string
}) {
  const theme = useTheme()
  return (
    <Card sx={{ p: 2 }}>
      <Typography
        variant="subtitle2"
        color={theme.palette.grey[600]}
        paragraph
        sx={{ display: 'flex', alignItems: 'center' }}
      >
        {title}
        {helpText && (
          <Tooltip title={helpText}>
            <Help
              sx={{
                fontSize: 16,
                ml: 1,
                color: (theme) => theme.palette.grey[500],
              }}
            />
          </Tooltip>
        )}
      </Typography>

      <Grid container>
        <Grid item xs={12}>
          <Typography variant="h4" paragraph sx={{ mb: 0.5, color: textColor }}>
            {children}
          </Typography>
          <Typography
            fontWeight="bold"
            borderRadius="20px"
            padding="2px 15px"
            textAlign="center"
            variant="caption"
            sx={{
              backgroundColor: theme.palette.primary.dark,
              display: 'inline-block',
            }}
          />
        </Grid>
      </Grid>
    </Card>
  )
}
