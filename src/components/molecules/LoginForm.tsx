import React from 'react'
import { Icon } from '@iconify/react'
import eyeFill from '@iconify/icons-eva/eye-fill'
import {
  Controller,
  DeepMap,
  FieldValues,
  FieldError,
  Control,
} from 'react-hook-form'
import eyeOffFill from '@iconify/icons-eva/eye-off-fill'
import { Stack, TextField, IconButton, InputAdornment } from '@material-ui/core'
import { LoadingButton } from '@mui/lab'

interface ILoginForm {
  onSubmit: React.FormEventHandler<HTMLFormElement> | undefined
  errors: DeepMap<FieldValues, FieldError>
  control: Control<FieldValues, object>
  showPassword: boolean
  handleShowPassword: () => void
  isSubmitting: boolean
}

function LoginForm({
  onSubmit,
  errors,
  control,
  showPassword,
  handleShowPassword,
  isSubmitting,
}: ILoginForm) {
  return (
    <form autoComplete="off" noValidate onSubmit={onSubmit}>
      <Stack spacing={3}>
        <Controller
          defaultValue=""
          render={({ field }) => (
            <TextField
              {...field}
              fullWidth
              autoComplete="username"
              type="string"
              label="Correo Electrónico"
              error={Boolean(errors?.username)}
              helperText={errors?.username?.message}
            />
          )}
          name="username"
          control={control}
          rules={{ required: true }}
        />

        <Controller
          name="password"
          control={control}
          rules={{ required: true }}
          render={({ field }) => (
            <TextField
              fullWidth
              autoComplete="current-password"
              type={showPassword ? 'text' : 'password'}
              label="Contraseña"
              InputProps={{
                endAdornment: (
                  <InputAdornment position="end">
                    <IconButton onClick={handleShowPassword} edge="end">
                      <Icon icon={showPassword ? eyeFill : eyeOffFill} />
                    </IconButton>
                  </InputAdornment>
                ),
              }}
              error={Boolean(errors.password)}
              helperText={errors?.password?.message}
              {...field}
            />
          )}
        />
      </Stack>

      <LoadingButton
        fullWidth
        size="large"
        type="submit"
        variant="contained"
        loading={isSubmitting}
        sx={{ my: 2 }}
      >
        Iniciar Sesión
      </LoadingButton>
    </form>
  )
}

export default LoginForm
