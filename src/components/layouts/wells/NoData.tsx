import { Box, Typography } from '@mui/material'
import React from 'react'

export default function NoData() {
  return (
    <Box
      sx={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        maxWidth: 800,
        margin: '0 auto',
        gap: 3,
        textAlign: 'center',
      }}
    >
      <img
        src="/static/illustrations/undraw-no-data.svg"
        width={250}
        alt="No hay datos"
      />
      <Typography variant="h4">
        Parece que no hay pozos asignados a este cliente. Si cree que se trata
        de un error contacte a su administrador.
      </Typography>
    </Box>
  )
}
