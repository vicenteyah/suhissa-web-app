import { Box, Typography } from '@mui/material'
import { useTheme } from '@material-ui/core/styles'
import React from 'react'

export default function NoLocation() {
  const theme = useTheme()
  return (
    <Box
      sx={{
        height: '24rem',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-evenly',
      }}
    >
      <img
        src="/static/illustrations/undraw-lost.svg"
        height={200}
        width="100%"
        alt="Coordenadas no disponibles"
      />
      <Typography
        sx={{ mt: 2, ml: 3 }}
        color={theme.palette.primary.dark}
        variant="h4"
        paragraph
      >
        Coordenadas no disponibles
      </Typography>
    </Box>
  )
}
