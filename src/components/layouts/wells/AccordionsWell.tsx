import { Well } from '@customTypes/entities/well'
import { Grid } from '@mui/material'
import AccordionWellDetails from 'components/layouts/wells/AccordionWellDetails'
import React from 'react'

type AccordionsWellProps = {
  well: Well
}

export default function AccordionsWell({ well }: AccordionsWellProps) {
  return (
    <Grid container sx={{ my: 2 }}>
      <AccordionWellDetails
        objects={['consesion', 'pumpingInfrastructure']}
        well={well}
      />
      <AccordionWellDetails
        objects={['accumulatedVolume', 'conductivity']}
        well={well}
      />
    </Grid>
  )
}
