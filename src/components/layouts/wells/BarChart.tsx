import { useTheme } from '@mui/material'
import React from 'react'
import dynamic from 'next/dynamic'
import { ApexOptions } from 'apexcharts'
import formatNumber from 'utils/functions/formatNumber'

type BarChartProps = {
  categories: string[]
  data: number[]
  title: string
  showYAxis: boolean
}

const ReactApexChart = dynamic(() => import('react-apexcharts'), { ssr: false })

export default function BarChart({
  categories,
  data,
  title,
  showYAxis,
}: BarChartProps) {
  const theme = useTheme()

  const chartData: ApexOptions = {
    chart: {
      type: 'bar',
      id: 'apexchart-example',
      width: '100%',
      foreColor: theme.palette.primary.main,
      zoom: {
        enabled: false,
      },
    },
    responsive: [
      {
        breakpoint: 768,
        options: {
          chart: {
            width: '1000px',
          },
        },
      },
    ],
    plotOptions: {
      bar: {
        borderRadius: 10,
        columnWidth: '50%',
        dataLabels: {
          position: 'top',
        },
      },
    },
    dataLabels: {
      enabled: true,
      formatter: (val) => {
        return formatNumber(typeof val === 'number' ? (val - val%(.01)) : 0)
      },
      offsetY: -15,
      style: {
        fontSize: '12px',
        colors: [theme.palette.grey[600]],
      },
      background: {
        enabled: false,
        dropShadow: {
          enabled: true,
        },
      },
    },
    xaxis: {
      categories: categories,
      labels: {
        rotate: -35,
        rotateAlways: true,
        hideOverlappingLabels: false,
      },
      tickPlacement: 'on',
    },
    yaxis: {
      labels: {
        show: showYAxis,
        formatter(val: number) {
          return formatNumber(val)
        },
      },
    },
    stroke: {
      show: false,
    },
    legend: {
      width: 350,
    },
    title: {
      text: title,
      align: 'center',
      style: {
        color: theme.palette.grey[700],
      },
    },
    series: [
      {
        name: title,
        type: 'column',
        data: data,
      },
    ],
  }

  return (
    <ReactApexChart
      height={350}
      width={data.length > 25 ? 2000: '100%'}
      options={chartData}
      series={chartData.series}
      style={{ overflowX: 'scroll', overflowY: 'hidden' }}
    />
  )
}
