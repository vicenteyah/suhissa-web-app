import { useEffect, useState, useContext } from 'react'
// material
import {
  Table,
  TableRow,
  TableBody,
  TableCell,
  TableContainer,
  Typography,
} from '@material-ui/core'
import { useTheme } from '@mui/material'
// components

import LinkReport from 'components/Generador/LinkReport'
import formatNumber from 'utils/functions/formatNumber'
import { WellPerDay } from '@customTypes/entities/well'
import SkeletonTableContent from 'components/layouts/skeletons/TableContent'
import { SettingsContext } from 'contexts/SettingsContext'
import generateDownloadUrl from 'api/downloadIndividualReport'
import moment from 'moment'
import useLocalStorage from 'hooks/useLocalStorage'
import SortingSelectingHead from './SortingSelectingHead'
import Scrollbar from '../../../molecules/Scrollbar'

// ----------------------------------------------------------------------

function createData(
  id: number,
  volumeId: number,
  flowmeterId: number,
  volumeFlowmeterId: number,
  date: Date | string,
  volume: number,
  conductivity: number,
  sdt: number,
) {
  return {
    id,
    volumeId,
    flowmeterId,
    volumeFlowmeterId,
    date,
    volume,
    conductivity,
    sdt,
  }
}

type headLabel = {
  id: string
  numeric: boolean
  align: 'center' | 'left' | 'right'
  disablePadding: boolean
  label: string
}

type SortItem = {
  id: number
  volumeId: number
  flowmeterId: number
  volumeFlowmeterId: number
  date: Date | string
  volume: number
  conductivity: number
  sdt: number
}

const TABLE_HEAD: headLabel[] = [
  {
    id: 'date',
    numeric: false,
    align: 'left',
    disablePadding: true,
    label: 'Fecha',
  },
  {
    id: 'volume',
    numeric: true,
    align: 'right',
    disablePadding: true,
    label: 'Volumen (m3)',
  },
  {
    id: 'conductivity',
    numeric: true,
    align: 'right',
    disablePadding: true,
    label: 'Conductividad (µS)',
  },
  {
    id: 'sdt',
    numeric: true,
    align: 'right',
    disablePadding: true,
    label: 'SDT (mg/lt)',
  },
]

// ----------------------------------------------------------------------

type Anonymous = Record<string | number, string>

function descendingComparator(a: Anonymous, b: Anonymous, orderBy: string) {
  if (b[orderBy] < a[orderBy]) {
    return -1
  }
  if (b[orderBy] > a[orderBy]) {
    return 1
  }
  return 0
}

function getComparator(order: string, orderBy: string) {
  return order === 'desc'
    ? (a: Anonymous, b: Anonymous) => descendingComparator(a, b, orderBy)
    : (a: Anonymous, b: Anonymous) => -descendingComparator(a, b, orderBy)
}

function stableSort(array: SortItem[], comparator: (a: any, b: any) => number) {
  const stabilizedThis = array.map((el, index) => [el, index] as const)
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0])
    if (order !== 0) return order
    return a[1] - b[1]
  })
  return stabilizedThis.map((el) => el[0])
}

type DetailsProps = {
  well: WellPerDay[] | undefined
  handleDate: (date: null | Date) => void
  wellsPerDayChanged: boolean
}

export default function SortingSelecting({
  well,
  handleDate,
  wellsPerDayChanged,
}: DetailsProps) {
  const [order, setOrder] = useState<'asc' | 'desc'>('asc')
  const [orderBy, setOrderBy] = useState('calories')
  const [wellsPerDay, setWellsPerDay] = useState<SortItem[]>([])
  const [date, setDate] = useState<null | Date | string>('')
  const { clientSelected } = useContext(SettingsContext)
  const [userToken, setToken] = useLocalStorage('token', '')

  useEffect(() => {
    if (well) {
      const wells = well.map((element: WellPerDay, index: number) => {
        if (index === 0) {
          const newDate = element.date.toString().split('-')
          setDate(`${newDate[0]}-${newDate[1]}`)
        }
        const data = createData(
          element.ceReadId,
          element.volumeReadId,
          element.ceId,
          element.volumeId,
          element.date.toString().split('T')[0],
          Number(element.volume),
          Number(element.ce),
          Number(element.sdt),
        )
        return data
      })
      setWellsPerDay(wells)
    }
  }, [well])

  const handleRequestSort = (property: string) => {
    const isAsc = orderBy === property && order === 'asc'
    setOrder(isAsc ? 'desc' : 'asc')
    setOrderBy(property)
  }

  const generateIndividualDownloadUrl = (
    flowmeterId: number,
    conductivityId: number,
  ) => {
    const url = generateDownloadUrl(
      clientSelected,
      String(userToken),
      flowmeterId,
      conductivityId,
    )
    return url
  }

  return (
    <div id="wells-table-container">
      <Scrollbar>
        <TableContainer sx={{ minWidth: 800 }}>
          <Table size="small">
            <SortingSelectingHead
              order={order}
              orderBy={orderBy}
              headLabel={TABLE_HEAD}
              onRequestSort={handleRequestSort}
              shownDate={date ? new Date(date.toString()) : new Date()}
              handleDate={handleDate}
            />
            {well?.length !== 0 ? (
              <TableBody>
                {wellsPerDayChanged ? (
                  stableSort(wellsPerDay, getComparator(order, orderBy)).map(
                    (row, index) => {
                      const labelId = `enhanced-table-checkbox-${index}`

                      return (
                        <TableRow hover key={labelId}>
                          <TableCell padding="none" colSpan={1}>
                            <LinkReport
                              ceReport={
                                row.id
                                  ? generateIndividualDownloadUrl(
                                      row.flowmeterId,
                                      row.id,
                                    )
                                  : ''
                              }
                              volumeReport={
                                row.volumeId
                                  ? generateIndividualDownloadUrl(
                                      row.volumeFlowmeterId,
                                      row.volumeId,
                                    )
                                  : ''
                              }
                              detailwells={`/dashboard/detailwells/?nmx=${row.volumeFlowmeterId}&simtca=${row.flowmeterId}&date=${row.date}`}
                            />
                          </TableCell>
                          <TableCell align="left">
                            {moment(row.date).format('DD/MM/YYYY')}
                          </TableCell>
                          <TableCell align="right">
                            {row.volume ? `${formatNumber(row.volume)}` : '-'}
                          </TableCell>
                          <TableCell align="right">
                            {row.conductivity
                              ? `${formatNumber(row.conductivity)}`
                              : '-'}
                          </TableCell>
                          <TableCell align="right">
                            {row.sdt ? `${formatNumber(row.sdt)}` : '-'}
                          </TableCell>
                        </TableRow>
                      )
                    },
                  )
                ) : (
                  <SkeletonTableContent />
                )}
              </TableBody>
            ) : (
              <caption>
                <Typography
                  variant="h3"
                  sx={{
                    textAlign: 'center',
                  }}
                >
                  No hay datos para mostrar.
                </Typography>
              </caption>
            )}
          </Table>
        </TableContainer>
      </Scrollbar>
    </div>
  )
}
