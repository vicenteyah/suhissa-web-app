import { Flowmeter } from '@customTypes/entities/Flowmeter'
import {
  Table,
  TableRow,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
} from '@mui/material'
import { getMonthConsumptionByDay } from 'api/getMonthConsumptionByDay'
import { getYearVolume } from 'api/getYearVolume'
import SkeletonTableContent from 'components/layouts/skeletons/TableContent'
import { SettingsContext } from 'contexts/SettingsContext'
import useLocalStorage from 'hooks/useLocalStorage'
import { useContext, useState, useEffect } from 'react'
import Scrollbar from '../../../molecules/Scrollbar'
import DashboardTableBody from './DashboardTableBody'

type DashboardTableProps = {
  wells: Flowmeter[]
}

type FlowmeterInfo = {
  id: number
  name: string
  latitude: number
  longitude: number
  idNumber: string
  type: string
  operationType: string
  nsm: string
  concession: string
  concessionedVolume: string
  concessionedVolumeAttached: string
  attached: string
  unloadDiameter: string
  hp: string
  pumpType: string
  pumpVoltage: string
  pipeType: string
  pipeDiameter: string
  brand: string
  model: string
  nsut: string
  nsue: string
  brandm: string
  modelm: string
  consumption: number
  consumptionByDay: number
}

export default function DashboardTable({ wells }: DashboardTableProps) {
  const [wellConsumption, setWellConsumption] = useState<FlowmeterInfo[]>([])
  const [wellsDataReady, setWellsDataReady] = useState<boolean>(false)
  const { clientSelected } = useContext(SettingsContext)
  const [userToken, setUserToken] = useLocalStorage('token', '')

  const getDailyConsumption = async (flowmeterId: number) => {
    try {
      const consumption = await getMonthConsumptionByDay(
        clientSelected,
        flowmeterId,
        String(userToken),
        new Date().getFullYear(),
        new Date().getMonth() + 1,
      )
      return consumption[0].consumption
    } catch (error) {
      return 0
    }
  }

  const getConsumption = async (flowmeterId: number, flowmeterType: string) => {
    try {
      let consumption = 0
      let consumptionByDay = 0
      if (flowmeterType === 'NMX') {
        consumption = await getYearVolume(
          flowmeterId,
          String(userToken),
          new Date().getFullYear(),
        )
        consumption = consumption > 0 ? consumption : 0

        consumptionByDay = await getDailyConsumption(flowmeterId)
        consumptionByDay = consumptionByDay > 0 ? consumptionByDay : 0
      }
      const wellIndex = wells.findIndex((well) => well.id === flowmeterId)
      return { ...wells[wellIndex], consumption, consumptionByDay }
    } catch (error) {
      const consumption = 0
      const consumptionByDay = 0
      const wellIndex = wells.findIndex((well) => well.id === flowmeterId)
      return { ...wells[wellIndex], consumption, consumptionByDay }
    }
  }

  useEffect(() => {
    if (wells.length > 0) {
      setWellsDataReady(false)
      const data = wells.map(async (well) => getConsumption(well.id, well.type))
      Promise.all(data).then((newData) => {
        setWellConsumption(newData)
        setWellsDataReady(true)
      })
    }
  }, [wells])

  return (
    <Scrollbar>
      <TableContainer sx={{ minWidth: 800 }}>
        <Table size="small">
          <TableHead>
            <TableRow>
              <TableCell align="left">Pozos</TableCell>
              <TableCell align="right">Flujo diario (m3)</TableCell>
              <TableCell align="right">Consumo (m3)</TableCell>
              <TableCell align="right">Vol. consecionado (m3)</TableCell>
              <TableCell align="right">Disponibilidad (m3)</TableCell>
              <TableCell align="center">%</TableCell>
              <TableCell align="center">Gráfica</TableCell>
            </TableRow>
          </TableHead>
          {wellsDataReady ? (
            <DashboardTableBody wells={wellConsumption} />
          ) : (
            <TableBody>
              <SkeletonTableContent />
            </TableBody>
          )}
        </Table>
      </TableContainer>
    </Scrollbar>
  )
}
