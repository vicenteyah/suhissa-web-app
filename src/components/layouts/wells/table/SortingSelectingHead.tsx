import { visuallyHidden } from '@material-ui/utils'
import {
  Box,
  TableRow,
  TableCell,
  TableHead,
  TableSortLabel,
} from '@material-ui/core'
import { TextField, TextFieldProps } from '@mui/material'
import React from 'react'
import { DatePicker, LocalizationProvider } from '@mui/x-date-pickers'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns'
import esLocale from 'date-fns/locale/es'

type SortingSelectingHeadProps = {
  orderBy: string
  shownDate: Date | null
  onRequestSort: (property: string) => void
  handleDate: (date: Date | null) => void
  order: 'asc' | 'desc'
  headLabel: {
    id: string
    numeric: boolean
    align: 'center' | 'left' | 'right'
    disablePadding: boolean
    label: string
  }[]
}

export default function SortingSelectingHead({
  order,
  orderBy,
  headLabel,
  shownDate,
  onRequestSort,
  handleDate,
}: SortingSelectingHeadProps) {
  const [date, setDate] = React.useState<Date | null>(shownDate)
  return (
    <TableHead>
      <TableRow>
        <TableCell colSpan={1}>
          <LocalizationProvider
            dateAdapter={AdapterDateFns}
            adapterLocale={esLocale}
          >
            <DatePicker
              views={['year', 'month']}
              label="Mes visualizado"
              minDate={new Date('2012-03-01')}
              maxDate={new Date()}
              value={date}
              onChange={(newValue) => {
                setDate(newValue)
                handleDate(newValue)
              }}
              renderInput={(params: TextFieldProps) => (
                <TextField {...params} helperText={null} />
              )}
            />
          </LocalizationProvider>
        </TableCell>
        {headLabel.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.align}
            sortDirection={orderBy === headCell.id ? order : false}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={() => onRequestSort(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <Box component="span" sx={{ ...visuallyHidden }}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </Box>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  )
}
