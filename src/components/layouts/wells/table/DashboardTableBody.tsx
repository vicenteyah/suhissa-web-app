import {
  TableRow,
  TableBody,
  TableCell,
} from '@mui/material'
import { useTheme } from '@material-ui/core/styles'
import Link from 'next/link'
import WellsAvailabilityChart from 'components/layouts/dashboard/general/WellsAvailabilityChart'
import formatNumber from 'utils/functions/formatNumber'

type FlowmeterInfo = {
  id: number
  name: string
  latitude: number
  longitude: number
  idNumber: string
  type: string
  operationType: string
  nsm: string
  concession: string
  concessionedVolume: string
  concessionedVolumeAttached: string
  attached: string
  unloadDiameter: string
  hp: string
  pumpType: string
  pumpVoltage: string
  pipeType: string
  pipeDiameter: string
  brand: string
  model: string
  nsut: string
  nsue: string
  brandm: string
  modelm: string
  consumption: number
  consumptionByDay: number
}

type TableBodyProps = {
  wells: FlowmeterInfo[]
}

export default function DashboardTableBody({ wells }: TableBodyProps) {
  const theme = useTheme()
  const calculateAvailability = (concessionedVolume: number, consumption: number) => {
    let availability = 0
    if (concessionedVolume > 0 && consumption > 0 && concessionedVolume > consumption) {
      availability = concessionedVolume - consumption
    }
      return formatNumber(availability)
  }

  const calculateAvailabilityPercentage = (
    concessionedVolume: number,
    consumption: number,
  ) => {
    let percentage = 0
    if (
      concessionedVolume > 0 &&
      consumption > 0 &&
      concessionedVolume > consumption
    ) {
      percentage = Number(
        (((concessionedVolume - consumption) * 100) / concessionedVolume).toFixed(1)
      )
    }
    return formatNumber(percentage)
  }

  return (
    <TableBody>
      {wells.map((wellInfo) => {
        return wellInfo.type === 'NMX' ? (
          <TableRow key={wellInfo.id}>
            <TableCell align="left">
              <Link href={`/dashboard/wells?well=${wellInfo.id}`}>
                <a style={{ color: theme.palette.primary.light }}>
                  {wellInfo.name}
                </a>
              </Link>
            </TableCell>
            <TableCell align="right">
              {formatNumber(wellInfo.consumptionByDay)}
            </TableCell>
            <TableCell align="right">
              {formatNumber(wellInfo.consumption)}
            </TableCell>
            <TableCell align="right">
              {wellInfo.concessionedVolumeAttached
                ? formatNumber(Number(wellInfo.concessionedVolumeAttached))
                : 0}
            </TableCell>
            <TableCell align="right">
              {calculateAvailability(
                Number(wellInfo.concessionedVolumeAttached),
                wellInfo.consumption,
              )}
            </TableCell>
            <TableCell align="center">
              {calculateAvailabilityPercentage(
                Number(wellInfo.concessionedVolumeAttached),
                wellInfo.consumption,
              )}{' '}
              %
            </TableCell>
            <TableCell align="center">
              <WellsAvailabilityChart
                series={Number(
                  calculateAvailabilityPercentage(
                    Number(wellInfo.concessionedVolumeAttached),
                    wellInfo.consumption,
                  ),
                )}
              />
            </TableCell>
          </TableRow>
        ) : (
          <TableRow key={wellInfo.id}>
            <TableCell align="left">
              <Link href={`/dashboard/wells?well=${wellInfo.id}`}>
                <a style={{ color: theme.palette.primary.light }}>
                  {wellInfo.name}
                </a>
              </Link>
            </TableCell>
            <TableCell align="center"> - </TableCell>
            <TableCell align="center"> - </TableCell>
            <TableCell align="center"> - </TableCell>
            <TableCell align="center"> - </TableCell>
            <TableCell align="center"> 0 % </TableCell>
            <TableCell align="center">
              <WellsAvailabilityChart
                series={Number(
                  calculateAvailabilityPercentage(
                    Number(wellInfo.concessionedVolumeAttached),
                    wellInfo.consumption,
                  ),
                )}
              />
            </TableCell>
          </TableRow>
        )
      })}
    </TableBody>
  )
}
