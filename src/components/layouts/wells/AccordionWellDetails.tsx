import { TextField, Typography } from '@material-ui/core'
import {
  Accordion,
  AccordionDetails,
  AccordionSummary,
  Box,
  Grid,
} from '@mui/material'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import { FieldKeys, FlowmeterLabel } from '@customTypes/entities/Flowmeter'
import { Well, getLabels } from '../../../@customTypes/entities/well'

type AccordionWellDetailsProps = {
  well: Well
  objects: FieldKeys[]
}

function getFieldTitle(field: FieldKeys) {
  let title = ''
  switch (field) {
    case 'consesion':
      title = 'Concesión'
      break
    case 'pumpingInfrastructure':
      title = 'Infraestructura de bombeo'
      break
    case 'accumulatedVolume':
      title = 'Volumen acumulado'
      break
    case 'conductivity':
      title = 'Conductividad'
      break
    default:
      title = ''
  }
  return title
}

function renderFields(fields: FlowmeterLabel[]) {
  return (
    <Grid>
      {fields.map((field) => (
        <Box key={field.title} sx={{ sm: { display: 'flex' } }}>
          <Typography variant="subtitle2">{field.title}</Typography>
          <Typography sx={{ mb: 1, display: 'block' }} variant="body2">
            {field.value}
          </Typography>
        </Box>
      ))}
    </Grid>
  )
}

export default function AccordionWellDetails({
  objects,
  well,
}: AccordionWellDetailsProps) {
  return (
    <Grid item xs={12} sx={{ mt: 1 }}>
      <Accordion>
        <AccordionSummary expandIcon={<ExpandMoreIcon />} id="accordion">
          <Grid container spacing={1}>
            {objects.map((object) => (
              <Grid key={object} item xs>
                <Typography
                  variant="h5"
                  sx={{ color: (theme) => theme.palette.primary.main }}
                >
                  {getFieldTitle(object)}
                </Typography>
              </Grid>
            ))}
          </Grid>
        </AccordionSummary>
        <AccordionDetails>
          <Grid container spacing={1}>
            {objects.map((object) => (
              <Grid key={object} item xs>
                {renderFields(getLabels(object, well))}
              </Grid>
            ))}
          </Grid>
        </AccordionDetails>
      </Accordion>
    </Grid>
  )
}
