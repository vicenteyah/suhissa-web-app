import React, { useEffect, useState } from 'react'
import Lightbox from 'react-image-lightbox'
import { Typography } from '@mui/material'
import { useTheme, styled } from '@material-ui/core/styles'

const IndexStyle = styled('div')(() => ({
  zIndex: 9900,
}))

type WellsPhotosProps = {
  photos: string[]
}

export default function WellsPhotos({ photos }: WellsPhotosProps) {
  const NoPhoto = '/static/illustrations/no-photos-well.svg'
  const [open, setStatus] = useState(false)
  const [photoIndex, setPhotoIndex] = useState(0)
  const theme = useTheme()

  function changePhotoIndexPrev() {
    setPhotoIndex((photoIndex + photos.length - 1) % photos.length)
  }

  function changePhotoNext() {
    setPhotoIndex((photoIndex + 1) % photos.length)
  }

  useEffect(() => {
    if (photos.length <= 0) {
      photos.push(NoPhoto)
    }
  }, [])

  return (
    <div>
      <Typography variant="button">
        <a
          style={{ color: theme.palette.primary.light }}
          href="#"
          onClick={() => setStatus(true)}
        >
          Ver fotografías
        </a>
      </Typography>

      {open && (
        <IndexStyle>
          <Lightbox
            mainSrc={photos[photoIndex]}
            nextSrc={photos[(photoIndex + 1) % photos.length]}
            prevSrc={photos[(photoIndex + photos.length - 1) % photos.length]}
            onCloseRequest={() => setStatus(false)}
            onMovePrevRequest={() => changePhotoIndexPrev()}
            onMoveNextRequest={() => changePhotoNext()}
          />
        </IndexStyle>
      )}
    </div>
  )
}
