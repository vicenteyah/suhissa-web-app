import { Well, getFlowmeter } from '@customTypes/entities/well'
import { Card, Grid, Typography } from '@mui/material'
import DataBox from 'components/molecules/DataBox'
import React, { useEffect, useState } from 'react'
import { useTheme } from '@material-ui/core/styles'
import GhostChip from 'components/atoms/GhostChip'
import WellsPhotos from 'components/layouts/wells/photographs/WellsPhotos'
import DataBoxText from 'components/molecules/DataBoxText'
import moment from 'moment'
import AvailabilityChart from '../dashboard/general/AvailabilityChart'
import WellsLocation from '../dashboard/WellsLocation'
import 'moment/locale/es-mx'
import NoLocation from './NoLocation'
import NoAvailability from './NoAvailability'

type DetailsProps = {
  currentWell: Well
  yearVolume: number
  lastUpdate: string
  lastRead: number
  caudal: number
  conductivity: number
  sdt: number
  photos: string[]
}
export default function Details({
  currentWell,
  conductivity,
  lastUpdate,
  lastRead,
  caudal,
  yearVolume,
  sdt,
  photos,
}: DetailsProps) {
  const theme = useTheme()
  moment.locale('es-mx')

  const [defaultFlowmeter, setDefaultFlowmeter] = useState(
    getFlowmeter(currentWell),
  )

  const [concessionedVolume, setConcessionedVolume] = useState(0)

  const getAvailabiltyLevelPercentage = (
    volume: number,
    availabilty: number,
  ) => {
    const percentage = Number(((availabilty * 100) / volume).toFixed(1))

    let availability = ''
    switch (true) {
      case percentage >= 70:
        availability = 'Alta'
        break
      case percentage < 70 && percentage >= 30:
        availability = 'Media'
        break
      case percentage < 30 && percentage > 0:
        availability = 'Baja'
        break
      default:
        availability = 'No disponible'
    }
    return availability
  }

  useEffect(() => {
    setDefaultFlowmeter(getFlowmeter(currentWell))

    if (currentWell.NMX) {
      setConcessionedVolume(Number(currentWell.NMX.concessionedVolumeAttached))
    } else {
      setConcessionedVolume(0)
    }
  }, [currentWell])

  return (
    <div>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={4}>
          <DataBox
            title="Disponibilidad"
            value={concessionedVolume - yearVolume}
            dataUnity="m3"
            total={concessionedVolume || undefined}
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <DataBox
            title="Volumen Acumulado"
            value={yearVolume}
            dataUnity="m3"
            hideChart
            helpText="Volumen consumido durante todo el año fiscal"
            total={concessionedVolume || undefined}
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <DataBox
            title="Volumen Concesionado del Anexo"
            value={concessionedVolume}
            dataUnity="m3"
          />
        </Grid>
      </Grid>
      <Grid container spacing={2} sx={{ mt: 0.25 }}>
        <Grid item xs={6} sm={4}>
          {conductivity ? (
            <DataBox
              title="Conductividad (CE)"
              value={conductivity}
              dataUnity="µS"
            />
          ) : (
            <DataBoxText title="Conductividad (CE)" textColor="GrayText">
              <span>No Disponible</span>
            </DataBoxText>
          )}
        </Grid>
        <Grid item xs={6} sm={4}>
          {sdt ? (
            <DataBox
              title="Sólidos Disueltos Totales (SDT)"
              value={sdt}
              dataUnity="mg/l"
            />
          ) : (
            <DataBoxText
              title="Sólidos Disueltos Totales (SDT)"
              textColor="GrayText"
            >
              <span>No Disponible</span>
            </DataBoxText>
          )}
        </Grid>
        <Grid item xs={6} sm={4}>
          {caudal ? (
            <DataBox
              title="Caudal Promedio Diario"
              value={Number(caudal.toFixed(2))}
              dataUnity="m3/h"
            />
          ) : (
            <DataBoxText title="Caudal Promedio Diario" textColor="GrayText">
              <span>No Disponible</span>
            </DataBoxText>
          )}
        </Grid>
        <Grid item xs={6} sm={4}>
          {Number(lastRead) ? (
            <DataBox
              title="Última lectura del medidor"
              value={lastRead}
              dataUnity="m3"
            />
          ) : (
            <DataBoxText
              title="Última lectura del medidor"
              textColor="GrayText"
            >
              <span>No Disponible</span>
            </DataBoxText>
          )}
        </Grid>
        <Grid item xs={6} sm={4}>
          <DataBoxText
            title="Última actualización"
            helpText="Última lectura recibida del medidor"
            textColor={lastUpdate ? 'black' : 'GrayText'}
          >
            {lastUpdate
              ? moment(lastUpdate).format('DD [de] MMM [a las] HH:mm')
              : 'No Disponible'}
          </DataBoxText>
        </Grid>

        <Grid item xs={6} sm={4}>
          <DataBoxText title="Ficha técnica">
            <a
              style={{ color: theme.palette.primary.light }}
              href="http://www.iotam-zeus.com:8300/api/pdf/lorem.zip"
            >
              Descargar
            </a>
          </DataBoxText>
        </Grid>
      </Grid>

      <Grid container sx={{ mt: 0.25 }} spacing={3}>
        <Grid item xs={12} sm={5} sx={{ sm: { maxHeight: '20vh' } }}>
          <Card sx={{ p: 2 }}>
            <Typography sx={{ ml: 1 }} variant="subtitle2" paragraph>
              Disponibilidad
            </Typography>
            {concessionedVolume ? (
              <AvailabilityChart
                title=""
                volume={concessionedVolume}
                availability={concessionedVolume - yearVolume}
              />
            ) : (
              <NoAvailability />
            )}

            <GhostChip
              label={getAvailabiltyLevelPercentage(
                concessionedVolume,
                concessionedVolume - yearVolume,
              )}
              sx={{ mt: 2, display: 'flex' }}
            />
          </Card>
        </Grid>
        <Grid item xs={12} sm={7}>
          <Card>
            <Typography sx={{ mt: 2, ml: 3 }} variant="subtitle2" paragraph>
              Ubicación
            </Typography>
            {defaultFlowmeter.latitude && defaultFlowmeter.longitude ? (
              <WellsLocation
                wells={[defaultFlowmeter]}
                initialLatitude={defaultFlowmeter.latitude}
                initialLongitude={defaultFlowmeter.longitude}
                showPopUp={false}
                height="24rem"
              />
            ) : (
              <NoLocation />
            )}

            <Grid
              container
              sx={{ display: 'flex', justifyContent: 'space-evenly', my: 1 }}
            >
              {defaultFlowmeter.latitude && defaultFlowmeter.longitude && (
                <>
                  <Typography variant="button">{`Latitud: ${defaultFlowmeter.latitude}`}</Typography>
                  <Typography variant="button">{`Longitud: ${defaultFlowmeter.longitude}`}</Typography>
                </>
              )}
              <WellsPhotos photos={photos} />
            </Grid>
          </Card>
        </Grid>
      </Grid>
    </div>
  )
}
