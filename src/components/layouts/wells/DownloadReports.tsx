import {
  Box,
  Button,
  IconButton,
  Modal,
  TextField,
  Typography,
  FormControl,
  RadioGroup,
  FormControlLabel,
  Radio,
} from '@mui/material'
import React, { useEffect } from 'react'

import CloseIcon from '@mui/icons-material/Close'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns'
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider'
import { DesktopDatePicker } from '@mui/x-date-pickers'
import esLocale from 'date-fns/locale/es'
import moment from 'moment'
import useLocalStorage from 'hooks/useLocalStorage'

const style = {
  position: 'absolute' as 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  bgcolor: 'background.paper',
  borderRadius: 3,
  boxShadow: 24,
  p: 3,
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
}

type DownloadReportsProps = {
  volumeId: number
  userToken: string
  ceId: number
}

export default function DownloadReports({
  volumeId,
  userToken,
  ceId,
}: DownloadReportsProps) {
  const url = process.env.NEXT_PUBLIC_IP_API_URL

  const [open, setOpen] = React.useState(false)
  const [initDate, setInitDate] = React.useState<Date>(new Date())
  const [endDate, setEndDate] = React.useState<Date>(new Date())
  const [selection, setSelection] = React.useState(0)
  const [validDates, setValidDates] = React.useState(true)
  const handleOpen = () => setOpen(true)
  const handleClose = () => setOpen(false)

  const handleChangeInitDate = (newDate: Date) => {
    if (newDate > endDate) setValidDates(false)
    else setValidDates(true)
    setInitDate(newDate)
  }

  const handleChangeEndDate = (newDate: Date) => {
    if (newDate >= initDate) setValidDates(true)
    setEndDate(newDate)
  }

  const handleSelection = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSelection(Number((event.target as HTMLInputElement).value))
  }

  const downloadReport = () => {
    const startRange = moment(initDate)
    const endRange = moment(endDate)
    const link = `${url}flowmeters/${selection}/conductivities/range?startYear=${startRange.year()}&startMonth=${
      startRange.month() + 1
    }&startDay=${startRange.date()}&endYear=${endRange.year()}&endMonth=${
      endRange.month() + 1
    }&endDay=${endRange.date()}&token=${userToken}`

    return link
  }

  useEffect(() => {
    setSelection(volumeId || ceId)
  }, [volumeId, ceId, userToken])

  return (
    <Box sx={{ display: 'flex', justifyContent: 'center', my: 3 }}>
      <Button onClick={handleOpen} variant="contained" size="large">
        Descargar múltiples lecturas
      </Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <IconButton
            sx={{ position: 'absolute', right: 8, top: 8 }}
            onClick={handleClose}
          >
            <CloseIcon />
          </IconButton>
          <Typography variant="h6" paragraph textAlign="center">
            Descargar múltiples lecturas
          </Typography>
          <LocalizationProvider
            dateAdapter={AdapterDateFns}
            adapterLocale={esLocale}
          >
            <Box
              sx={{
                mt: 3,
                display: 'flex',
                gap: 4,
                width: '100%',
              }}
            >
              <DesktopDatePicker
                label="Fecha inicial"
                inputFormat="dd/MM/yyyy"
                maxDate={new Date()}
                value={initDate}
                onChange={(newValue) =>
                  handleChangeInitDate(newValue || new Date())
                }
                renderInput={(params) => <TextField {...params} />}
              />
              <DesktopDatePicker
                label="Fecha final"
                inputFormat="dd/MM/yyyy"
                value={endDate}
                minDate={initDate}
                maxDate={new Date()}
                onChange={(newValue) =>
                  handleChangeEndDate(newValue || new Date())
                }
                renderInput={(params) => <TextField {...params} />}
              />
            </Box>
          </LocalizationProvider>
          <FormControl sx={{ mt: 4 }}>
            <RadioGroup
              row
              name="row-radio-multiple-downloads"
              defaultValue={selection}
              value={selection}
              onChange={handleSelection}
              sx={{
                gap: 3,
              }}
            >
              <FormControlLabel
                value={volumeId}
                disabled={!volumeId}
                control={<Radio />}
                label="Volumen"
              />
              <FormControlLabel
                value={ceId}
                disabled={!ceId}
                control={<Radio />}
                label="Conductividad"
              />
            </RadioGroup>
          </FormControl>
          <Button
            size="large"
            sx={{ my: 3 }}
            variant="contained"
            href={downloadReport()}
            disabled={!validDates}
          >
            Descargar Lecturas
          </Button>
        </Box>
      </Modal>
    </Box>
  )
}
