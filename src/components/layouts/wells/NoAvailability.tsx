import { Box, Typography } from '@mui/material'

import { useTheme } from '@material-ui/core/styles'
import React from 'react'

export default function NoAvailability() {
  const theme = useTheme()
  return (
    <Box
      sx={{
        height: '23rem',
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'space-evenly',
      }}
    >
      <img
        src="/static/illustrations/undraw-pie-chart.svg"
        height={200}
        width="100%"
        alt="gráfico no disponible"
      />
      <Typography
        sx={{ mt: 2, ml: 3 }}
        color={theme.palette.primary.dark}
        variant="h4"
        paragraph
      >
        Gráfico no disponible
      </Typography>
    </Box>
  )
}
