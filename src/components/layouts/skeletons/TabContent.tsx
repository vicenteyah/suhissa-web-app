import { Box, Grid, Skeleton } from '@mui/material'
import React from 'react'

export default function TabContent() {
  return (
    <Box>
      <Skeleton
        variant="rectangular"
        height={60}
        sx={{ bgcolor: 'grey.300', borderRadius: 1, mt: 5, mb: 1 }}
      />
      <Skeleton
        variant="rectangular"
        height={60}
        sx={{ bgcolor: 'grey.300', borderRadius: 1, my: 1 }}
      />
      <Grid container spacing={2}>
        <Grid item xs={12} sm={4}>
          <Skeleton
            variant="rectangular"
            height={120}
            sx={{ bgcolor: 'grey.300', borderRadius: 1 }}
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <Skeleton
            variant="rectangular"
            height={120}
            sx={{ bgcolor: 'grey.300', borderRadius: 1 }}
          />
        </Grid>
        <Grid item xs={12} sm={4}>
          <Skeleton
            variant="rectangular"
            height={120}
            sx={{ bgcolor: 'grey.300', borderRadius: 1 }}
          />
        </Grid>
        <Grid item xs={6} sm={4}>
          <Skeleton
            variant="rectangular"
            height={100}
            sx={{ bgcolor: 'grey.300', borderRadius: 1 }}
          />
        </Grid>
        <Grid item xs={6} sm={4}>
          <Skeleton
            variant="rectangular"
            height={100}
            sx={{ bgcolor: 'grey.300', borderRadius: 1 }}
          />
        </Grid>
        <Grid item xs={6} sm={4}>
          <Skeleton
            variant="rectangular"
            height={100}
            sx={{ bgcolor: 'grey.300', borderRadius: 1 }}
          />
        </Grid>
        <Grid item xs={6} sm={4}>
          <Skeleton
            variant="rectangular"
            height={100}
            sx={{ bgcolor: 'grey.300', borderRadius: 1 }}
          />
        </Grid>
        <Grid item xs={6} sm={4}>
          <Skeleton
            variant="rectangular"
            height={100}
            sx={{ bgcolor: 'grey.300', borderRadius: 1 }}
          />
        </Grid>
        <Grid item xs={6} sm={4}>
          <Skeleton
            variant="rectangular"
            height={100}
            sx={{ bgcolor: 'grey.300', borderRadius: 1 }}
          />
        </Grid>
      </Grid>
    </Box>
  )
}
