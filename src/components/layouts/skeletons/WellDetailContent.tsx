import { Grid, Skeleton } from '@mui/material'
import React from 'react'
import BarChart from './BarChart'

export default function WellDetailContent() {
  return (
    <Grid container spacing={2}>
      <Grid item xs={12} sm={6}>
        <Skeleton
          variant="rectangular"
          height={120}
          sx={{ bgcolor: 'grey.300', borderRadius: 1 }}
        />
      </Grid>
      <Grid item xs={12} sm={6}>
        <Skeleton
          variant="rectangular"
          height={120}
          sx={{ bgcolor: 'grey.300', borderRadius: 1 }}
        />
      </Grid>
      <Grid item xs={12} sm={6}>
        <Skeleton
          variant="rectangular"
          height={120}
          sx={{ bgcolor: 'grey.300', borderRadius: 1 }}
        />
      </Grid>
      <Grid item xs={12} sm={6}>
        <Skeleton
          variant="rectangular"
          height={120}
          sx={{ bgcolor: 'grey.300', borderRadius: 1 }}
        />
      </Grid>
      <Grid item xs={12}>
        <BarChart />
      </Grid>
    </Grid>
  )
}
