import { Box, Grid, Skeleton, TableCell, TableRow } from '@mui/material'
import React from 'react'

function TableIconsContent() {
  return (
    <Box>
      <Grid container>
        <Grid item xs={6} sm={2}>
          <Skeleton
            variant="circular"
            height={40}
            width={40}
            sx={{ bgcolor: 'grey.300' }}
          />
        </Grid>
        <Grid item xs={6} sm={2}>
          <Skeleton
            variant="circular"
            height={40}
            width={40}
            sx={{ bgcolor: 'grey.300' }}
          />
        </Grid>
        <Grid item xs={6} sm={2}>
          <Skeleton
            variant="circular"
            height={40}
            width={40}
            sx={{ bgcolor: 'grey.300' }}
          />
        </Grid>
      </Grid>
    </Box>
  )
}

export default function TableContent() {
  return (
    <>
      {[...Array(3)].map((item, index) => (
        <TableRow key={`loading-reads-${index + 1}`}>
          <TableCell padding="normal" colSpan={5}>
            <TableIconsContent />
          </TableCell>
          <TableCell padding="normal" colSpan={5}>
            <Box>
              <Grid container spacing={1}>
                <Grid item xs={6} sm={3}>
                  <Skeleton
                    variant="text"
                    sx={{ bgcolor: 'grey.300', borderRadius: 1 }}
                  />
                </Grid>
                <Grid item xs={6} sm={3}>
                  <Skeleton
                    variant="text"
                    sx={{ bgcolor: 'grey.300', borderRadius: 1 }}
                  />
                </Grid>
                <Grid item xs={6} sm={3}>
                  <Skeleton
                    variant="text"
                    sx={{ bgcolor: 'grey.300', borderRadius: 1 }}
                  />
                </Grid>
                <Grid item xs={6} sm={3}>
                  <Skeleton
                    variant="text"
                    sx={{ bgcolor: 'grey.300', borderRadius: 1 }}
                  />
                </Grid>
              </Grid>
            </Box>
          </TableCell>
        </TableRow>
      ))}
    </>
  )
}
