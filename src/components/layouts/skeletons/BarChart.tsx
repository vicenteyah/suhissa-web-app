import React from 'react'
import { Box, Skeleton } from '@mui/material'

function BarChart() {
  return (
    <Box
      sx={{
        display: 'flex',
        gap: 2,
        margin: 4,
        justifyContent: 'center',
        alignItems: 'flex-end',
      }}
    >
      <Skeleton
        variant="rectangular"
        width={35}
        height={100}
        sx={{ bgcolor: 'grey.300', borderRadius: '10px 10px 0 0' }}
      />
      <Skeleton
        variant="rectangular"
        width={35}
        height={150}
        sx={{ bgcolor: 'grey.300', borderRadius: '10px 10px 0 0' }}
      />
      <Skeleton
        variant="rectangular"
        width={35}
        height={200}
        sx={{ bgcolor: 'grey.300', borderRadius: '10px 10px 0 0' }}
      />
      <Skeleton
        variant="rectangular"
        width={35}
        height={300}
        sx={{ bgcolor: 'grey.300', borderRadius: '10px 10px 0 0' }}
      />
      <Skeleton
        variant="rectangular"
        width={35}
        height={200}
        sx={{ bgcolor: 'grey.300', borderRadius: '10px 10px 0 0' }}
      />
    </Box>
  )
}

BarChart.metadata = {
  name: 'Phuong Dao',
  github: 'dao-phuong',
  description: 'Bar Chart',
  filename: 'BarChart',
}

export default BarChart
