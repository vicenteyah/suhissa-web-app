import { Box, Grid, Skeleton, TableCell, TableRow } from '@mui/material'
import React from 'react'

export default function HourlyTableContent() {
  return (
    <>
      {[...Array(3)].map((item, index) => (
        <TableRow key={`loading-reads-${index + 1}`}>
          <TableCell padding="normal" colSpan={5}>
            <Box>
              <Grid container spacing={1}>
                <Grid item xs={6} sm={3}>
                  <Skeleton
                    variant="text"
                    sx={{ bgcolor: 'grey.300', borderRadius: 1 }}
                  />
                </Grid>
                <Grid item xs={6} sm={3}>
                  <Skeleton
                    variant="text"
                    sx={{ bgcolor: 'grey.300', borderRadius: 1 }}
                  />
                </Grid>
                <Grid item xs={6} sm={3}>
                  <Skeleton
                    variant="text"
                    sx={{ bgcolor: 'grey.300', borderRadius: 1 }}
                  />
                </Grid>
                <Grid item xs={6} sm={3}>
                  <Skeleton
                    variant="text"
                    sx={{ bgcolor: 'grey.300', borderRadius: 1 }}
                  />
                </Grid>
              </Grid>
            </Box>
          </TableCell>
        </TableRow>
      ))}
    </>
  )
}
