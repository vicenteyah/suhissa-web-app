import { Box, Skeleton } from '@mui/material'
import React from 'react'

export default function Tabs() {
  return (
    <Box
      sx={{
        display: 'flex',
        border: 1,
        p: 0.5,
        borderRadius: 2,
        borderColor: 'divider',
      }}
    >
      {[...Array(3)].map((e, i) => (
        <Skeleton
          key={`tab-skeleton-${i + 1}`}
          variant="rectangular"
          width={75}
          height={25}
          sx={{ borderRadius: 20, bgcolor: 'grey.300', m: 1 }}
        />
      ))}
    </Box>
  )
}
