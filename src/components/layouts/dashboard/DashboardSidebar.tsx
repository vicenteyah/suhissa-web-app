import { useEffect } from 'react'
import NextLink from 'next/link'
import { useRouter } from 'next/router'
import { alpha, styled } from '@material-ui/core/styles'
import { Box, Stack, Avatar, Drawer, Typography } from '@material-ui/core'
import useCollapseDrawer from 'hooks/useCollapseDrawer'
import { getAvatarInitials } from 'utils/functions/getAvatarInitials'
import Scrollbar from '../../molecules/Scrollbar'
import NavSection from '../../molecules/NavSection'
import { MHidden } from '../../@material-extend'
import sidebarConfig from './SidebarConfig'

const DRAWER_WIDTH = 280
const COLLAPSE_WIDTH = 102

const RootStyle = styled('div')(({ theme }) => ({
  [theme.breakpoints.up('lg')]: {
    flexShrink: 0,
    transition: theme.transitions.create('width', {
      duration: theme.transitions.duration.complex,
    }),
  },
}))

const AccountStyle = styled('div')(({ theme }) => ({
  display: 'flex',
  alignItems: 'center',
  padding: theme.spacing(2, 2.5),
  borderRadius: theme.shape.borderRadiusSm,
  backgroundColor: theme.palette.grey[50024],
}))

type DashboardSidebarProps = {
  isOpenSidebar: boolean
  onCloseSidebar: VoidFunction
}

export default function DashboardSidebar({
  isOpenSidebar,
  onCloseSidebar,
}: DashboardSidebarProps) {
  const { pathname } = useRouter()

  const {
    isCollapse,
    collapseClick,
    collapseHover,
    onHoverEnter,
    onHoverLeave,
  } = useCollapseDrawer()

  useEffect(() => {
    if (isOpenSidebar) {
      onCloseSidebar()
    }
  }, [pathname])

  const renderContent = (
    <Scrollbar
      sx={{
        backgroundColor: (theme) => theme.palette.primary.main,
        height: '100%',
        '& .simplebar-content': {
          height: '100%',
          display: 'flex',
          flexDirection: 'column',
        },
      }}
    >
      <Stack
        spacing={3}
        sx={{
          px: 2.5,
          pt: 3,
          pb: 2,
          ...(isCollapse && {
            alignItems: 'center',
          }),
        }}
      >
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="space-between"
        >
          <NextLink href="/">
            <Box
              component="img"
              src="/static/brand/logo_full_white.png"
              sx={{
                display: 'inline-flex',
                cursor: 'pointer',
                width: '180px',
                margin: '0 auto',
              }}
            />
          </NextLink>
        </Stack>

        {isCollapse ? (
          <Avatar
            alt="My Avatar"
            sx={{
              mx: 'auto',
              mb: 2,
              bgcolor: (theme) => theme.palette.primary.dark,
            }}
          >
            {getAvatarInitials('Manuel López')}
          </Avatar>
        ) : (
          <NextLink href="#">
            <AccountStyle>
              <Avatar
                alt="My Avatar"
                sx={{
                  bgcolor: (theme) => theme.palette.primary.lighter,
                  color: 'white',
                }}
              >
                {getAvatarInitials('Manuel López')}
              </Avatar>
              <Box sx={{ ml: 2, overflowX: 'hidden' }}>
                <Typography
                  variant="subtitle2"
                  sx={{ color: (theme) => theme.palette.grey[100] }}
                >
                  Manuel López
                </Typography>
                <Typography
                  variant="caption"
                  sx={{
                    color: (theme) => theme.palette.grey[400],
                  }}
                >
                  manuel@nativodigital.com
                </Typography>
              </Box>
            </AccountStyle>
          </NextLink>
        )}
      </Stack>

      <NavSection navConfig={sidebarConfig} isShow={!isCollapse} />
    </Scrollbar>
  )

  return (
    <RootStyle
      sx={{
        width: {
          lg: isCollapse ? COLLAPSE_WIDTH : DRAWER_WIDTH,
        },
        ...(collapseClick && {
          position: 'absolute',
        }),
      }}
    >
      <MHidden width="lgUp">
        <Drawer
          open={isOpenSidebar}
          onClose={onCloseSidebar}
          PaperProps={{
            sx: { width: DRAWER_WIDTH },
          }}
        >
          {renderContent}
        </Drawer>
      </MHidden>

      <MHidden width="lgDown">
        <Drawer
          open
          variant="persistent"
          onMouseEnter={onHoverEnter}
          onMouseLeave={onHoverLeave}
          PaperProps={{
            sx: {
              width: DRAWER_WIDTH,
              bgcolor: 'background.default',
              ...(isCollapse && {
                width: COLLAPSE_WIDTH,
              }),
              ...(collapseHover && {
                borderRight: 0,
                backdropFilter: 'blur(6px)',
                WebkitBackdropFilter: 'blur(6px)', // Fix on Mobile
                boxShadow: (theme) => theme.customShadows.z20,
                bgcolor: (theme) =>
                  alpha(theme.palette.background.default, 0.88),
              }),
            },
          }}
        >
          {renderContent}
        </Drawer>
      </MHidden>
    </RootStyle>
  )
}
