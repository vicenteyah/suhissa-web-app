import React, { useEffect, useState } from 'react'
import Map, { Marker, Popup, NavigationControl } from 'react-map-gl'
import WebMercatorViewport from 'viewport-mercator-project'
import Place from '@mui/icons-material/Place'
import PopupWellLocation from 'components/molecules/PopupWellLocation'
import { Flowmeter } from '@customTypes/entities/Flowmeter'

const TOKEN = process.env.NEXT_PUBLIC_MAP_KEY

type WellsLocationProps = {
  wells: Flowmeter[]
  initialLatitude: number
  initialLongitude: number
  height: string
  showPopUp: boolean
}

export default function WellsLocation({
  wells,
  initialLatitude,
  initialLongitude,
  height,
  showPopUp,
}: WellsLocationProps) {
  const [popupInfo, setPopupInfo] = useState<Flowmeter | undefined>(undefined)
  const [viewport, setViewport] = useState({
    latitude: initialLatitude, // wells.length > 1 ? 23.6260333 : wells[0].latitude,
    longitude: initialLongitude, // wells.length > 1 ? -102.5375005 : wells[0].longitude,
    zoom: wells.length > 1 ? 4 : 13,
  })

  const onMove = React.useCallback(({ viewState }) => {
    setViewport({
      latitude: viewState.latitude,
      longitude: viewState.longitude,
      zoom: viewState.zoom,
    })
  }, [])

  useEffect(() => {
    setViewport({
      zoom: wells.length > 1 ? 4 : 13,
      latitude: initialLatitude,
      longitude: initialLongitude,
    })
  }, [initialLatitude, initialLongitude, wells.length])

  const renderPopup = () =>
    popupInfo && (
      <Popup
        anchor="bottom"
        longitude={popupInfo.longitude}
        latitude={popupInfo.latitude}
        closeOnClick={false}
        onClose={() => setPopupInfo(undefined)}
      >
        <PopupWellLocation well={popupInfo} />
      </Popup>
    )

  const renderCityMarker = (flowmeter: Flowmeter, index: number) => {
    return (
      <Marker
        anchor="center"
        key={`marker-${index}`}
        latitude={flowmeter.latitude}
        longitude={flowmeter.longitude}
        onClick={() => setPopupInfo(flowmeter)}
      >
        <Place color="secondary" fontSize="large" />
      </Marker>
    )
  }

  return (
    <Map
      initialViewState={viewport}
      {...viewport}
      onMove={onMove}
      style={{
        height: height,
        borderRadius: '8px',
      }}
      scrollZoom={false}
      boxZoom
      mapStyle="mapbox://styles/mapbox/streets-v11"
      mapboxAccessToken={TOKEN}
    >
      <NavigationControl />
      {wells.map((well, index) => renderCityMarker(well, index))}
      {showPopUp && renderPopup()}
    </Map>
  )
}
