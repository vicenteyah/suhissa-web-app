import { ReactNode, useState } from 'react'
import { styled, useTheme } from '@material-ui/core/styles'

import { Typography } from '@mui/material'
import DashboardNavbar from './DashboardNavbar'
import DashboardSidebar from './DashboardSidebar'

const APP_BAR_MOBILE = 64
const APP_BAR_DESKTOP = 92

const RootStyle = styled('div')(({ theme }) => ({
  minHeight: '100%',
  overflow: 'hidden',
}))

const MainStyle = styled('div')(({ theme }) => ({
  flexGrow: 1,
  overflow: 'auto',
  minHeight: '100%',
  paddingTop: APP_BAR_MOBILE,
  paddingBottom: theme.spacing(10),
  [theme.breakpoints.up('lg')]: {
    paddingTop: APP_BAR_DESKTOP,
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    marginLeft: '280px',
  },
}))

const Footer = styled(Typography)(({ theme }) => ({
  marginBottom: '2em',
  marginRight: '2em',
  fontSize: '13px',
  color: '#000',
  textAlign: 'right',
  fontWeight: 'bold',
}))

type DashboardLayoutProps = {
  children?: ReactNode
}

export default function DashboardLayout({ children }: DashboardLayoutProps) {
  const theme = useTheme()
  const [open, setOpen] = useState(false)

  return (
    <RootStyle>
      <DashboardNavbar onOpenSidebar={() => setOpen(true)} />
      <DashboardSidebar
        isOpenSidebar={open}
        onCloseSidebar={() => setOpen(false)}
      />

      <MainStyle
        sx={{
          transition: theme.transitions.create('margin', {
            duration: theme.transitions.duration.complex,
          }),
        }}
      >
        {children}
      </MainStyle>
      <Footer>
        Powered by{' '}
        <a
          href="https://suhissa.com.mx"
          style={{ color: '#4a5fa4' }}
          target="_blank"
          rel="noreferrer"
        >
          SUHISSA
        </a>
        {' | '}
        <a
          href="https://suhissa.com.mx/contacto/"
          style={{ color: '#4a5fa4' }}
          target="_blank"
          rel="noreferrer"
        >
          Soporte
        </a>
      </Footer>
    </RootStyle>
  )
}
