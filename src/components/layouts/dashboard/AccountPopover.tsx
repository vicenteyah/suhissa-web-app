import { Icon } from '@iconify/react'
import { useRef, useState } from 'react'
import homeFill from '@iconify/icons-eva/home-fill'
import personFill from '@iconify/icons-eva/person-fill'
import dropletFill from '@iconify/icons-eva/droplet-fill'
import settings2Fill from '@iconify/icons-eva/settings-2-fill'
import OpacityOutlined from '@mui/icons-material/OpacityOutlined'
import NextLink from 'next/link'
import { alpha } from '@material-ui/core/styles'
import {
  Box,
  Avatar,
  Button,
  Divider,
  MenuItem,
  Typography,
} from '@material-ui/core'
import useUserService from 'services/factories/UserService'
import { PATH_DASHBOARD } from 'routes/paths'
import { getAvatarInitials } from 'utils/functions/getAvatarInitials'
import MenuPopover from '../../molecules/MenuPopover'
import { MIconButton } from '../../@material-extend'

const MENU_OPTIONS = [
  /* { label: 'Dashboard', icon: homeFill, linkTo: PATH_DASHBOARD.root }, */
  {
    label: 'Pozos',
    icon: dropletFill,
    linkTo: PATH_DASHBOARD.general.wells,
  },
]

export default function AccountPopover() {
  const { logoutUser } = useUserService()
  const anchorRef = useRef(null)
  const [open, setOpen] = useState(false)

  const handleOpen = () => {
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }

  async function logout() {
    try {
      logoutUser()
    } catch (error) {
      alert(error)
    }
  }

  return (
    <>
      <MIconButton
        ref={anchorRef}
        onClick={handleOpen}
        sx={{
          padding: 0,
          width: 44,
          height: 44,
          ...(open && {
            '&:before': {
              zIndex: 1,
              content: "''",
              width: '100%',
              height: '100%',
              borderRadius: '50%',
              position: 'absolute',
              bgcolor: (theme) => alpha(theme.palette.grey[900], 0.32),
            },
          }),
        }}
      >
        <Avatar
          alt="My Avatar"
          sx={{
            bgcolor: (theme) => theme.palette.primary.main,
            color: 'white',
          }}
        >
          {getAvatarInitials('Manuel López')}
        </Avatar>
      </MIconButton>

      <MenuPopover
        open={open}
        onClose={handleClose}
        anchorEl={anchorRef.current}
        sx={{ width: 220 }}
      >
        <Box sx={{ my: 1.5, px: 2.5 }}>
          <Typography variant="subtitle1" noWrap>
            Manuel López
          </Typography>
          <Typography variant="body2" sx={{ color: 'text.secondary' }} noWrap>
            manuel@nativodigital.mx
          </Typography>
        </Box>

        <Divider sx={{ my: 1 }} />

        {MENU_OPTIONS.map((option) => (
          <NextLink key={option.label} href={option.linkTo}>
            <MenuItem
              onClick={handleClose}
              sx={{ typography: 'body2', py: 1, px: 2.5 }}
            >
              <Box
                component={Icon}
                icon={option.icon}
                sx={{
                  mr: 2,
                  width: 24,
                  height: 24,
                }}
              />

              {option.label}
            </MenuItem>
          </NextLink>
        ))}

        <Box sx={{ p: 2, pt: 1.5 }}>
          <Button fullWidth color="error" variant="contained" onClick={logout}>
            Cerrar sesión
          </Button>
        </Box>
      </MenuPopover>
    </>
  )
}
