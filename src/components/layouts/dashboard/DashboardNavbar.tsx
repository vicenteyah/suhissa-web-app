import { Icon } from '@iconify/react'
import menu2Fill from '@iconify/icons-eva/menu-2-fill'

import { alpha, styled } from '@material-ui/core/styles'
import { Box, Stack, AppBar, Toolbar, IconButton } from '@material-ui/core'

import { Typography, useTheme } from '@mui/material'
import { SettingsContext } from 'contexts/SettingsContext'
import { useContext, useEffect, useState } from 'react'
import SelectClient from 'components/molecules/SelectClient'
import { If, Then } from 'react-if'
import { MHidden } from '../../@material-extend'
import AccountPopover from './AccountPopover'

const DRAWER_WIDTH = 280
const APPBAR_MOBILE = 64
const APPBAR_DESKTOP = 92

const RootStyle = styled(AppBar)(({ theme }) => ({
  boxShadow: 'none',
  backdropFilter: 'blur(6px)',
  borderBottomColor: theme.palette.grey[300],
  borderBottomWidth: '1px',
  borderBottomStyle: 'solid',
  WebkitBackdropFilter: 'blur(6px)', // Fix on Mobile
  backgroundColor: alpha(theme.palette.background.default, 0.82),
  [theme.breakpoints.up('lg')]: {
    width: `calc(100% - ${DRAWER_WIDTH}px)`,
  },
}))

const ToolbarStyle = styled(Toolbar)(({ theme }) => ({
  minHeight: APPBAR_MOBILE,
  [theme.breakpoints.up('lg')]: {
    minHeight: APPBAR_DESKTOP,
    padding: theme.spacing(0, 5),
  },
}))

type DashboardNavbarProps = {
  onOpenSidebar: VoidFunction
}

export default function DashboardNavbar({
  onOpenSidebar,
}: DashboardNavbarProps) {
  const [path, setPath] = useState('')
  const { wellSelected } = useContext(SettingsContext)

  useEffect(() => {
    setPath(window.location.pathname)
  }, [])

  return (
    <RootStyle>
      <ToolbarStyle>
        <MHidden width="lgUp">
          <IconButton
            onClick={onOpenSidebar}
            sx={{ mr: 1, color: 'text.primary' }}
          >
            <Icon icon={menu2Fill} />
          </IconButton>
        </MHidden>

        <Box sx={{ flexGrow: 1 }}>
          <Typography variant="h3" color="black">
            {wellSelected}
          </Typography>
        </Box>

        <Stack
          direction="row"
          alignItems="center"
          justifyContent="center"
          spacing={{ xs: 2, sm: 4 }}
          flexWrap="nowrap"
        >
          <If condition={path !== '/configuration/account'}>
            <Then>
              <SelectClient />
            </Then>
          </If>
          <AccountPopover />
        </Stack>
      </ToolbarStyle>
    </RootStyle>
  )
}
