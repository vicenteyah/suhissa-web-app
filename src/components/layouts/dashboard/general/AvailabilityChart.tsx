import { Card, useTheme } from '@mui/material'
import React from 'react'
import dynamic from 'next/dynamic'
import formatNumber from 'utils/functions/formatNumber'
import { ApexOptions } from 'apexcharts'

type AvailabilityChartProps = {
  title: string
  volume: number
  availability: number
}

const ReactApexChart = dynamic(() => import('react-apexcharts'), { ssr: false })

export default function AvailabilityChart({
  volume,
  availability,
  title,
}: AvailabilityChartProps) {
  const theme = useTheme()

  const series = [availability, volume - availability]

  const options: ApexOptions = {
    chart: {
      type: 'pie',
    },
    legend: {
      position: 'bottom',
    },
    labels: ['Disponible', 'Totalizadores'],
    theme: {
      monochrome: {
        enabled: true,
        color: theme.palette.primary.dark,
      },
    },
    plotOptions: {
      pie: {
        dataLabels: {
          offset: -20,
        },
      },
    },
    title: {
      text: title,
    },
    tooltip: {
      custom({ series, seriesIndex, dataPointIndex, w }) {
        const amount = series[seriesIndex]
        const label = w.globals.labels[seriesIndex]

        return `<span style="padding: 8px"> ${label}: ${formatNumber(
          amount,
        )} </span>`
      },
    },
    dataLabels: {
      formatter(val: number, opts) {
        const amount = opts.w.globals.series[opts.seriesIndex]
        return [formatNumber(amount), `${val.toFixed(1)}%`] as unknown as
          | string
          | number
      },
    },
  }

  return (
    <ReactApexChart
      height="400px"
      options={options}
      series={series}
      type="pie"
    />
  )
}
