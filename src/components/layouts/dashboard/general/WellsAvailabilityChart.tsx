import { useTheme } from '@mui/material'
import React from 'react'
import dynamic from 'next/dynamic'
import formatNumber from 'utils/functions/formatNumber'
import { ApexOptions } from 'apexcharts'

const ReactApexChart = dynamic(() => import('react-apexcharts'), { ssr: false })

type WellsAvailabilityChartProps = {
  annotationColor?: string
  series: number
}

type ColorFunctionProp = {
  value: number
}

export default function WellsAvailabilityChart({
  annotationColor,
  series,
}: WellsAvailabilityChartProps) {
  const theme = useTheme()

  const yaxis = [series]

  const options: ApexOptions = {
    chart: {
      type: 'radialBar',
      sparkline: {
        enabled: true,
      },
      offsetY: 0,
      offsetX: 40,
    },
    responsive: [
      {
        breakpoint: 1500,
        options: {
          chart: {
            offsetX: 0,
            width: '50%',
            height: '50%',
          },
        },
      },
      {
        breakpoint: 920,
        options: {
          chart: {
            offsetY: 0,
            width: '70%',
            height: '50%',
          },
        },
      },
    ],
    plotOptions: {
      radialBar: {
        startAngle: -90,
        endAngle: 90,
        track: {
          background: '#e7e7e7',
          strokeWidth: '97%',
        },
        dataLabels: {
          name: {
            show: false,
          },
          value: {
            show: false,
          },
        },
      },
    },
    fill: {
      type: 'solid',
    },
    colors: [
      function ({ value }: ColorFunctionProp) {
        if (value > 70) {
          return '#3CE862'
        }
        if (value > 30 && value < 70) {
          return '#FBF324'
        }
        return '#D9534F'
      },
    ],
    yaxis: {
      labels: {
        formatter(val: number) {
          return formatNumber(val)
        },
      },
    },
    tooltip: {
      y: {
        formatter(val: number) {
          const mount = formatNumber(val)
          return mount === 'NaN' ? 'Sin Datos' : mount
        },
      },
    },
  }

  return (
    <ReactApexChart
      options={options}
      series={yaxis}
      type="radialBar"
      height="55%"
      width="35%"
    />
  )
}
