import { Card, useTheme } from '@mui/material'
import React from 'react'
import dynamic from 'next/dynamic'
import formatNumber from 'utils/functions/formatNumber'
import { ApexOptions } from 'apexcharts'

const ReactApexChart = dynamic(() => import('react-apexcharts'), { ssr: false })

type LineChartProps = {
  annotationColor?: string
  title: string
  titleAlign?: 'left' | 'right' | 'center' | undefined
  series: number[]
  categories: string[]
  seriesLabel: string
}

export default function LineChart({
  annotationColor,
  title,
  titleAlign = 'center',
  series,
  categories,
  seriesLabel,
}: LineChartProps) {
  const theme = useTheme()

  const yaxis = [
    {
      name: seriesLabel,
      data: series,
    },
  ]

  const options: ApexOptions = {
    chart: {
      type: 'line',
      zoom: {
        enabled: false,
      },
    },
    dataLabels: {
      formatter(val: number) {
        return formatNumber(val)
      },
      enabled: true,
      offsetX: 5,
      offsetY: 5,
      style: {
        fontSize: '11px',
      },
      enabledOnSeries: [0],
      background: {
        enabled: true,
        foreColor: '#000',
        opacity: 0,
        dropShadow: {
          enabled: false,
        },
      },
    },
    title: {
      text: title,
      align: titleAlign,
    },
    colors: [
      theme.palette.primary.light,
      annotationColor || theme.palette.primary.dark,
    ],
    grid: {
      row: {
        colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
        opacity: 0.5,
      },
    },
    xaxis: {
      categories: categories,
    },
    yaxis: {
      labels: {
        formatter(val: number) {
          return formatNumber(val)
        },
      },
    },
    tooltip: {
      y: {
        formatter(val: number) {
          const mount = formatNumber(val)
          return mount === 'NaN' ? 'Sin Datos' : mount
        },
      },
    },
  }

  return (
    <ReactApexChart
      options={options}
      series={yaxis}
      type="line"
      height="350px"
    />
  )
}
