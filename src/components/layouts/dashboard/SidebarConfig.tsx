import OpacityOutlined from '@mui/icons-material/OpacityOutlined'
import PersonOutline from '@mui/icons-material/Person'
import Speed from '@mui/icons-material/Speed'
import { PATH_DASHBOARD } from '../../../routes/paths'

interface ISidebarConfigElements {
  subheader: string
  items: {
    title: string
    path: string
    icon: JSX.Element
    children?: {
      title: string
      path: string
    }[]
  }[]
}

const sidebarConfig: ISidebarConfigElements[] = [
  {
    subheader: 'general',
    items: [
      /* {
        title: 'Dashboard',
        path: PATH_DASHBOARD.general.dashboard,
        icon: <Speed />,
      }, */
      {
        title: 'Pozos',
        path: PATH_DASHBOARD.general.wells,
        icon: <OpacityOutlined />,
      },
    ],
  },
]

export default sidebarConfig
