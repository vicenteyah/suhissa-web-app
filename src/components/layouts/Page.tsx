import Head from 'next/head'
import { forwardRef, ReactNode } from 'react'
import { Box, BoxProps, Typography } from '@material-ui/core'
import { styled } from '@material-ui/core/styles'

interface PageProps extends BoxProps {
  children: ReactNode
  title?: string
}

const Page = forwardRef<HTMLDivElement, PageProps>(
  ({ children, title = '', ...other }, ref) => (
    <Box ref={ref} {...other}>
      <Head>
        <title>{title} - SITMCO</title>
      </Head>
      {children}
    </Box>
  ),
)

export default Page
