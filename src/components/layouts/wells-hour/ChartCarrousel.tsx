import React, { useEffect, useState } from 'react'
import Slider from 'react-slick'
import 'slick-carousel/slick/slick.css'
import 'slick-carousel/slick/slick-theme.css'
import { Box, Typography } from '@mui/material'
import { HourlyRead } from '@customTypes/entities/HourlyRead'
import BarChart from '../wells/BarChart'
import LineChart from '../dashboard/general/LineChart'

const settings = {
  dots: true,
  infinite: true,
  swipeToSlide: false,
  slidesToShow: 1,
  slidesToScroll: 1,
}

type ChartCarrouselProps = {
  labels: string[]
  data: HourlyRead[]
}

export default function ChartCarrousel({ labels, data }: ChartCarrouselProps) {
  const [consumeData, setConsumeData] = useState<number[]>([])
  const [caudalData, setCaudalData] = useState<number[]>([])
  const [conductivityData, setConductivityData] = useState<number[]>([])
  const [sdtData, setSdtData] = useState<number[]>([])

  const getIndicators = (attribute: keyof HourlyRead, reads: HourlyRead[]) => {
    let mapped: number[] = []
    if (reads.some((read) => read[attribute] > 0)) {
      mapped = reads.map((read) => Number(read[attribute]) || 0)
    }
    return mapped
  }

  useEffect(() => {
    setConsumeData(getIndicators('consume', data))
    setCaudalData(getIndicators('caudal', data))
    setConductivityData(getIndicators('ce', data))
    setSdtData(getIndicators('sdt', data))
  }, [data])
  return (
    <Box sx={{ mt: 4 }}>
      {data.length > 0 && (
        <Slider {...settings}>
          {consumeData.length > 0 && (
            <BarChart
              categories={labels}
              data={consumeData}
              showYAxis
              title="Consumo Horario"
            />
          )}
          {caudalData.length > 0 && (
            <LineChart
              title="Caudal"
              series={caudalData}
              categories={labels}
              seriesLabel="Caudal"
            />
          )}
          {conductivityData.length > 0 && (
            <LineChart
              title="Conductividad"
              series={conductivityData}
              categories={labels}
              seriesLabel="Conductividad"
            />
          )}
          {sdtData.length > 0 && (
            <LineChart
              title="Sólidos Disueltos Totales"
              series={sdtData}
              categories={labels}
              seriesLabel="SDT"
            />
          )}
        </Slider>
      )}
    </Box>
  )
}
