import * as React from 'react'
import Table from '@mui/material/Table'
import TableBody from '@mui/material/TableBody'
import TableCell from '@mui/material/TableCell'
import TableContainer from '@mui/material/TableContainer'
import TableHead from '@mui/material/TableHead'
import TableRow from '@mui/material/TableRow'
import Paper from '@mui/material/Paper'
import Typography from '@mui/material/Typography'
import formatNumber from 'utils/functions/formatNumber'
import { DatePicker, LocalizationProvider } from '@mui/x-date-pickers'
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns'
import esLocale from 'date-fns/locale/es'
import { TextField, TextFieldProps } from '@mui/material'
import SkeletonTableContent from 'components/layouts/skeletons/HourlyTableContent'
import { useEffect } from 'react'
import { HourlyRead } from '@customTypes/entities/HourlyRead'
import { If, Then } from 'react-if'

type DataHourTableProps = {
  data: HourlyRead[]
  labels: string[]
  handleDate: (date: Date | null) => void
  datePicker: Date
  detailsByHourChanged: boolean
}

export default function DataHourTable({
  data,
  labels,
  handleDate,
  datePicker,
  detailsByHourChanged,
}: DataHourTableProps) {
  const [date, setDate] = React.useState<Date | null>(new Date(datePicker))

  useEffect(() => {
    setDate(datePicker)
  }, [datePicker])

  return (
    <TableContainer component={Paper} sx={{ mt: 7 }}>
      <Table sx={{ minWidth: 850 }} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell align="center">
              <LocalizationProvider
                dateAdapter={AdapterDateFns}
                adapterLocale={esLocale}
              >
                <DatePicker
                  label="Día visualizado"
                  maxDate={new Date()}
                  value={date}
                  onChange={(newValue) => {
                    setDate(newValue)
                    handleDate(newValue)
                  }}
                  renderInput={(params: TextFieldProps) => (
                    <TextField {...params} helperText={null} />
                  )}
                />
              </LocalizationProvider>
            </TableCell>
            <TableCell align="center">Consumo Horario (m3)</TableCell>
            <TableCell align="right">Volumen Acumulado (m3)</TableCell>
            <TableCell align="right">Caudal (m3/h)</TableCell>
            <TableCell align="right">Conductividad (µs/cm2)</TableCell>
            <TableCell align="right">SDT (mg/lt)</TableCell>
          </TableRow>
        </TableHead>
        <If condition={detailsByHourChanged && data.length > 0}>
          <Then>
            <TableBody>
              {data.map((row, index) => (
                <TableRow
                  hover
                  key={row.id}
                  sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                >
                  <TableCell
                    component="th"
                    scope="row"
                    align="center"
                    sx={{ pl: '4rem !important' }}
                  >
                    {`${labels[index]}`}
                  </TableCell>
                  <TableCell align="right">
                    {row.consume
                      ? `${formatNumber(row.consume ? row.consume : 0)}`
                      : '-'}
                  </TableCell>
                  <TableCell align="right">
                    {row.volume ? `${formatNumber(row.volume)}` : '-'}
                  </TableCell>
                  <TableCell align="right">
                    {row.caudal ? `${formatNumber(row.caudal)}` : '-'}
                  </TableCell>
                  <TableCell align="right">
                    {row.ce ? `${formatNumber(row.ce)}` : '-'}
                  </TableCell>
                  <TableCell align="right">
                    {row.sdt ? `${formatNumber(row.sdt)}` : '-'}
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Then>
        </If>
        <If condition={detailsByHourChanged && data.length === 0}>
          <Then>
            <caption>
              <Typography
                variant="h3"
                sx={{
                  textAlign: 'center',
                }}
              >
                No hay datos para mostrar.
              </Typography>
            </caption>
          </Then>
        </If>
        <If condition={!detailsByHourChanged}>
          <Then>
            <SkeletonTableContent />
          </Then>
        </If>
      </Table>
    </TableContainer>
  )
}
