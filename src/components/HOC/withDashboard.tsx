import React, { ReactElement } from 'react'
import DashboardLayout from 'components/layouts/dashboard'
import Page from 'components/layouts/Page'
import { Container } from '@material-ui/core'
import useSettings from 'hooks/useSettings'

export const withDashboard: <T>(
  ContentComponent: React.ComponentType,
  pageName: string,
) => React.FC<T> = (ContentComponent, pageName) => (props) => {
  const { themeStretch } = useSettings()

  return (
    <DashboardLayout>
      <Page title={pageName}>
        <Container maxWidth={themeStretch ? false : 'xl'}>
          <ContentComponent {...props} />
        </Container>
      </Page>
    </DashboardLayout>
  )
}
