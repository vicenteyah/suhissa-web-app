import { Container, Stack, Tab } from '@material-ui/core'
import { TabContext, TabList } from '@mui/lab'
import { useRouter } from 'next/router'
import React, { useEffect, useMemo } from 'react'
import { When } from 'react-if'
import useTabsStore, { ITab, useSelectedTabStore } from 'store/tabStore'
import findTab from 'utils/functions/findTab'
import getLastItem from 'utils/functions/getLastItemFromPath'
import redirectWithoutRerenderTo from 'utils/functions/redirectWithoutRerenderTo'
import Close from '@mui/icons-material/Close'
import { v4 as uuidv4 } from 'uuid'
import MountedTabPanel from '../molecules/MountedTabPanel'

export const withTabs: <T>(
  ContentComponent: React.ComponentType,
  config: { tabName: string },
) => React.FC<T> = (ContentComponent, config) =>
  function (props) {
    const { pathname, push, asPath } = useRouter()
    const lastPath = useMemo(() => getLastItem(pathname), [pathname])
    const { tabs, addTab, removeTab, setTabComponent }: any = useTabsStore()
    const { selectedTab, setSelectedTab } = useSelectedTabStore()

    useEffect(() => {
      let foundTab = findTab(tabs, pathname)
      if (!foundTab) {
        foundTab = {
          id: uuidv4(),
          ContentComponent,
          componentPath: pathname,
          name: lastPath,
          tag: config.tabName,
        }
        addTab(foundTab)
      }
      if (!foundTab.ContentComponent) {
        setTabComponent(foundTab.id, ContentComponent)
      }
      setSelectedTab(foundTab)
    }, [asPath])

    const redirectToTab = (tab: ITab) => {
      if (tab.ContentComponent) {
        redirectWithoutRerenderTo(tab?.componentPath || '')
      } else {
        push(tab.componentPath)
      }
    }

    const handleChange = (event: React.SyntheticEvent, tabName: string) => {
      if (!(event.target instanceof HTMLButtonElement)) return
      const foundTab = findTab(tabs, tabName) as ITab
      setSelectedTab(foundTab)
      redirectToTab(foundTab)
    }

    const handleDelete = (tab: ITab) => (event: React.SyntheticEvent) => {
      const isToRemoveTabSelected = tab.id === selectedTab?.id
      const tabIndex = tabs.findIndex((item: any) => item.id === tab.id)
      if (isToRemoveTabSelected) {
        const tabToMoveTo = tabIndex === 0 ? tabs[1] : tabs[tabIndex - 1]
        setSelectedTab(tabToMoveTo)
        redirectToTab(tabToMoveTo)
      }
      removeTab(tab.id)
    }

    return (
      <TabContext value={selectedTab?.name || ''}>
        <Stack
          spacing={3}
          sx={{ borderBottom: 1, borderColor: 'divider', marginBottom: 3 }}
        >
          <TabList onChange={handleChange}>
            {tabs.map((tab: any) => (
              <Tab
                label={tab.tag}
                key={tab.name}
                value={tab.name}
                sx={{
                  flexDirection: 'row-reverse',
                  alignItems: 'center',
                  paddingRight: '-5px',
                }}
                icon={
                  <When condition={tabs.length > 1}>
                    <Close
                      id={tab.name}
                      key={tab.name}
                      onClick={handleDelete(tab)}
                      sx={{
                        height: '15px',
                        marginRight: '0px !important',
                      }}
                    />
                  </When>
                }
              />
            ))}
          </TabList>
        </Stack>
        <Container
          sx={{ position: 'relative', maxWidth: 'inherit !important' }}
        >
          {tabs.map((tab: any) => (
            <MountedTabPanel key={tab.name} value={tab.name}>
              <When condition={tab.ContentComponent !== undefined}>
                <tab.ContentComponent {...props} />
              </When>
            </MountedTabPanel>
          ))}
        </Container>
      </TabContext>
    )
  }
