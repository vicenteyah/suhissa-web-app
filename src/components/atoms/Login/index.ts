import { styled } from '@material-ui/core/styles'
import Page from 'components/layouts/Page'
import { Card, Box, BoxTypeMap, Typography } from '@material-ui/core'
import {
  CommonProps,
  OverridableComponent,
} from '@material-ui/core/OverridableComponent'

const RootStyle = styled(Page)(({ theme }) => ({
  [theme.breakpoints.up('md')]: {
    display: 'flex',
  },
}))

const SectionStyle = styled(Card)(({ theme }) => ({
  width: '100%',
  maxWidth: 475,
  display: 'flex',
  flexDirection: 'column',
  justifyContent: 'center',
  position: 'relative',
  borderRadius: '0px',
}))

const ContentStyle = styled('div')(({ theme }) => ({
  maxWidth: 480,
  margin: 'auto',
  display: 'flex',
  minHeight: '100vh',
  flexDirection: 'column',
  justifyContent: 'center',
  padding: theme.spacing(12, 0),
  paddingTop: '0',
}))

const LogoImage = styled(Box)(({ theme }) => ({
  height: 'auto',
  width: '300px',
  marginBottom: '60px',
  [theme.breakpoints.up('md')]: {
    alignSelf: 'auto',
  },
  [theme.breakpoints.down('md')]: {
    alignSelf: 'center',
  },
})) as OverridableComponent<BoxTypeMap<{}, 'div'>>

const BackgroundImage = styled(Box)(({ theme }) => ({
  backgroundImage: `url(${'/static/login/main.png'})`,
  backgroundSize: 'cover',
  height: '100%',
  width: 'auto',
}))

const textDefault: any = {
  color: 'white',
  textAlign: 'center',
  width: '100%',
}

const WelcomeTitle = styled(Typography)(({ theme }) => ({
  ...textDefault,
  position: 'absolute',
  top: '90px',
}))

const WelcomeParagraph = styled(Typography)(({ theme }) => ({
  ...textDefault,
  position: 'absolute',
  top: '152px',
  fontSize: '18px !important',
}))

const Footer = styled(Typography)(({ theme }) => ({
  ...textDefault,
  position: 'absolute',
  bottom: '20px',
  right: '20px',
  fontSize: '13px',
  color: '#000',
  textAlign: 'right',
  fontWeight: 'bold',
}))

export {
  SectionStyle,
  RootStyle,
  ContentStyle,
  LogoImage,
  WelcomeTitle,
  WelcomeParagraph,
  BackgroundImage,
  Footer,
}
