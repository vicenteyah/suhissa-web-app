import { Chip, SxProps } from '@mui/material'
import React from 'react'
import { useTheme } from '@material-ui/core/styles'

type ChipColor = 'success' | 'warning' | 'error'

type GhostChipProps = {
  label: string
  sx?: SxProps
}

export default function GhostChip({ label, sx }: GhostChipProps) {
  const theme = useTheme()
  
  const setColorChip = (): ChipColor => {
    let colorChip: ChipColor = 'success'

    if (label === 'Baja') colorChip = 'warning'
    if (label === 'No disponible') colorChip = 'error'

    return colorChip
  }

  return (
    <Chip
      variant="filled"
      label={label}
      sx={{
        ...sx,
        fontWeight: 'bold',
        borderRadius: '8px',
        backgroundColor: `${theme.palette[setColorChip()].lighter}`,
        color: theme.palette[setColorChip()].dark,
      }}
    />
  )
}
