import React from 'react'
// material
import { useTheme } from '@material-ui/core/styles'
import { Box, BoxProps } from '@material-ui/core'

// ----------------------------------------------------------------------

const Logo = React.forwardRef<any, BoxProps>(({ sx }, ref) => {
  const theme = useTheme()
  const PRIMARY_LIGHT = theme.palette.primary.light
  const PRIMARY_MAIN = theme.palette.primary.main
  const PRIMARY_DARK = theme.palette.primary.dark

  return (
    <Box ref={ref} sx={{ width: 40, height: 40, cursor: 'pointer', ...sx }}>
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="100%"
        height="100%"
        viewBox="0 0 374 245"
      >
        <path
          d="M34 100L0.5 83.5C7.3 20.3 54.6667 1.83333 77.5 0.5H302C353.2 8.9 370.667 52.6667 373 73.5H339.5C328.7 42.7 307.667 34.6667 298.5 34.5H75C36.6 44.1 31.6667 82.1667 34 100Z"
          fill="#E22029"
        />
        <path
          d="M0 165V116.5L34 133.5V165C39.6 199.4 65.3333 210.333 77.5 211.5H301C324.2 207.5 336.333 183.167 339.5 171.5C350.833 171.167 373.4 170.7 373 171.5C363 227.1 319.5 243.667 299 245H72.5C18.5 235.8 1.66667 187.833 0 165Z"
          fill="#294367"
        />
        <path
          d="M50.0001 121.5C37 113 51 99.4999 59.0001 108L77.5 108C159.5 63.5 241.333 93.6666 267.5 114.5H309C356.6 116.1 371.5 139.5 373 151C372.5 154.5 368.5 154.5 367 151.5C358.6 129.9 325.5 124.167 310 124C284.116 123.238 269.436 123.263 243 124C243 124 238 123.5 238 120C237 117 251.5 114.5 251.5 114.5C232.7 102.1 198.667 97.3333 184 96.5H152C139.6 94.9 108.833 106.167 95 112C102.667 111.833 121.7 111.6 136.5 112C151.3 112.4 159 125.5 161 132C164.5 138 155.857 145.39 153 136C150.5 125.5 139 121 134.5 121.5H50.0001Z"
          fill="#35363A"
        />
      </svg>
    </Box>
  )
})

export default Logo
