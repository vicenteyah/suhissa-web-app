import { PATH_LOGIN } from 'routes/paths'
import getSspWithSession from 'services/lib/getSspWithSession'
import { withDashboard } from 'components/HOC/withDashboard'
import { redirectIfNotAuth } from 'services/handlers/redirectIf'
import { Typography, Grid, Button } from '@material-ui/core'
import DataBox from 'components/molecules/DataBox'
import React, { useEffect, useState } from 'react'
import DataBoxText from 'components/molecules/DataBoxText'
import { SettingsContext } from 'contexts/SettingsContext'
import ChartCarrousel from 'components/layouts/wells-hour/ChartCarrousel'
import DataHourTable from 'components/layouts/wells-hour/DataHourTable'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import { getWellDetailsByHour } from 'api/getWellDetailsByHour'
import moment, { Moment } from 'moment'
import { HourlyRead } from '@customTypes/entities/HourlyRead'
import { getAllFlowmeterDetails } from 'api/getFlowmeterDetails'
import { Flowmeter } from '@customTypes/entities/Flowmeter'
import { getYearVolume } from 'api/getYearVolume'
import SkeletonWellDetailContent from 'components/layouts/skeletons/WellDetailContent'
import { If, Then } from 'react-if'
import SkeletonBarChart from 'components/layouts/skeletons/BarChart'
import useLocalStorage from 'hooks/useLocalStorage'

function DetailWells() {
  const [defaultFlowmeter, setDefaultFlowmeter] = useState<Flowmeter>()
  const [concessionedVolume, setConcessionedVolume] = useState(0)
  const [consumedVolume, setConsumedVolume] = useState(0)
  const [averageCaudal, setAverageCaudal] = useState(0)
  const [flowmeterLoading, setFlowmeterLoading] = useState(false)
  const [detailsByHour, setDetailsByHour] = useState<HourlyRead[]>([])
  const [labels, setLabels] = useState<string[]>([])
  const [simtcaId, setSimtcaId] = useState(0)
  const [nmxId, setNmxId] = useState(0)

  const [datePicker, setDatePicker] = useState<Date>(new Date())
  const [detailsByHourChanged, setDetailsByHourChanged] =
    useState<boolean>(false)

  const [userToken, setUserToken] = useLocalStorage('token', '')
  const { onSelectWell } = React.useContext(SettingsContext)

  const findReadByHour = (reads: HourlyRead[], hour: number) =>
    reads.find(
      (read) =>
        Number(moment.utc(read.readingDate).local().format('H')) === hour,
    )

  const getHourlyReads = async (flowmeterId: number, date: Moment) => {
    let reads: HourlyRead[] = []
    if (flowmeterId) {
      const response = await getWellDetailsByHour(
        flowmeterId,
        String(userToken),
        date.year(),
        date.month() + 1,
        date.date(),
      )
      reads = [...response]
    }
    return reads
  }

  const updateReads = async (
    date: Moment,
    volFlowmeterId = nmxId,
    ceFlowmeterId = simtcaId,
  ) => {
    const ceDetails = await getHourlyReads(ceFlowmeterId, date)
    const volumeDetails = await getHourlyReads(volFlowmeterId, date)

    const newDetails: HourlyRead[] = []
    for (let hour = 23; hour >= 0; hour -= 1) {
      const ceReadMatched = findReadByHour(ceDetails, hour)
      const volReadMatched = findReadByHour(volumeDetails, hour)

      if (ceReadMatched || volReadMatched) {
        newDetails.push({
          id: volReadMatched?.id || ceReadMatched?.id || 0,
          readingDate:
            volReadMatched?.readingDate || ceReadMatched?.readingDate || '',
          consume: 0,
          volume: volReadMatched?.volume || 0,
          caudal: volReadMatched?.caudal || 0,
          ce: ceReadMatched?.ce || 0,
          sdt: ceReadMatched?.sdt || 0,
        })
      }
    }

    const finalReads = newDetails.map((read, index) => ({
      ...read,
      consume: index > 0 ? newDetails[index - 1].volume - read.volume : 0,
    }))

    let caudalReads = 0
    const sumCaudal = finalReads.reduce((sum, read) => {
      if (read.caudal) caudalReads += 1
      return Number(sum) + Number(read.caudal)
    }, 0)
    if (caudalReads) {
      setAverageCaudal(sumCaudal / caudalReads)
    } else {
      setAverageCaudal(0)
    }

    setLabels(
      finalReads.map((data) =>
        moment.utc(data.readingDate).local().format('HH:00 [hrs]'),
      ),
    )

    return finalReads
  }

  const getWellDetails = async (
    date: Moment,
    volFlowmeterId: number,
    ceFlowmeterId: number,
  ) => {
    setFlowmeterLoading(true)
    setDatePicker(date.toDate())
    setDetailsByHourChanged(false)

    if (volFlowmeterId) {
      const volFlowmeter = await getAllFlowmeterDetails(
        volFlowmeterId,
        String(userToken),
      )
      onSelectWell(volFlowmeter.name)
      setDefaultFlowmeter(volFlowmeter)
      const conssesioned = Number(volFlowmeter.concessionedVolumeAttached) || 0
      setConcessionedVolume(conssesioned)
      if (conssesioned) {
        const yearVolume = await getYearVolume(
          volFlowmeterId,
          String(userToken),
          date.year(),
        )
        setConsumedVolume(Number(yearVolume) || 0)
        setAverageCaudal(135.23)
      }
    } else if (ceFlowmeterId) {
      const ceFlowmeter = await getAllFlowmeterDetails(
        ceFlowmeterId,
        String(userToken),
      )
      onSelectWell(ceFlowmeter.name)
      setDefaultFlowmeter(ceFlowmeter)
    }

    const hourlyReads = await updateReads(date, volFlowmeterId, ceFlowmeterId)

    setDetailsByHour(hourlyReads)

    setFlowmeterLoading(false)
    setDetailsByHourChanged(true)
  }

  const handleDate = async (date: Date | null) => {
    if (date) {
      setDetailsByHourChanged(false)
      const hourlyReads = await updateReads(moment(date))
      setDetailsByHour(hourlyReads)
      setDatePicker(date)
      setDetailsByHourChanged(true)
    }
  }

  useEffect(() => {
    if (userToken) {
      const urlParams = new URLSearchParams(window.location.search)
      const volFlowmeterId = Number(urlParams.get('nmx')) || 0
      setNmxId(volFlowmeterId)
      const ceFlowmeterId = Number(urlParams.get('simtca')) || 0
      setSimtcaId(ceFlowmeterId)
      const dateSelected =
        urlParams.get('date') || moment().format('YYYY-MM-DD')
      getWellDetails(moment(dateSelected), volFlowmeterId, ceFlowmeterId)
    }
  }, [userToken])

  return (
    <>
      <Typography
        variant="h4"
        sx={{ mb: 4, color: (theme) => theme.palette.grey[700] }}
      >
        Lectura horaria - {moment(datePicker).format('DD/MM/YYYY')}
      </Typography>
      <If condition={flowmeterLoading}>
        <Then>
          <SkeletonWellDetailContent />
        </Then>
      </If>
      <If condition={!flowmeterLoading}>
        <Then>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <DataBox
                title="Disponibilidad"
                value={concessionedVolume - consumedVolume}
                dataUnity="m3"
                total={concessionedVolume || undefined}
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              <DataBox
                title="Consumo m3"
                value={consumedVolume}
                dataUnity="m3"
                hideChart
                helpText="Volumen consumido durante todo el año fiscal"
                total={concessionedVolume || undefined}
              />
            </Grid>
          </Grid>
          <Grid container spacing={2} sx={{ mt: 1 }}>
            <Grid item xs={12} sm={6}>
              <DataBox
                title="Volumen Concesionado"
                value={concessionedVolume}
                dataUnity="m3"
              />
            </Grid>
            <Grid item xs={12} sm={6}>
              {averageCaudal ? (
                <DataBox
                  title="Caudal diario"
                  value={averageCaudal}
                  dataUnity="m3"
                />
              ) : (
                <DataBoxText title="Caudal diario" textColor="GrayText">
                  No Disponible
                </DataBoxText>
              )}
            </Grid>
          </Grid>
          {detailsByHourChanged ? (
            <ChartCarrousel data={detailsByHour} labels={labels} />
          ) : (
            <SkeletonBarChart />
          )}
          <DataHourTable
            data={detailsByHour}
            labels={labels}
            handleDate={handleDate}
            datePicker={datePicker}
            detailsByHourChanged={detailsByHourChanged}
          />
          <Button
            variant="text"
            sx={{ mt: 2, fontSize: 17 }}
            href={`/dashboard/wells?well=${
              defaultFlowmeter?.id || 0
            }&element=wells-table-container&date=${moment(datePicker).format(
              'YYYY-MM',
            )}`}
            startIcon={<ArrowBackIcon />}
          >
            Regresar
          </Button>
        </Then>
      </If>
    </>
  )
}

export default withDashboard(DetailWells, 'Pozos')

export const getServerSideProps = getSspWithSession(
  redirectIfNotAuth(PATH_LOGIN.root),
)
