import { PATH_LOGIN } from 'routes/paths'
import getSspWithSession from 'services/lib/getSspWithSession'
import { withDashboard } from 'components/HOC/withDashboard'
import { redirectIfNotAuth } from 'services/handlers/redirectIf'

import PanelItem from 'components/molecules/TabPanelItem'

import { Box, Tab } from '@mui/material'
import React, { useContext, useEffect } from 'react'

import { TabContext, TabList } from '@mui/lab'
import { SettingsContext } from 'contexts/SettingsContext'
import { getWellsByClient } from 'api/wellsByClient'
import SkeletonTabs from 'components/layouts/skeletons/Tabs'
import SkeletonTabContent from 'components/layouts/skeletons/TabContent'
import { Wells, Well, getFlowmeter, findWell } from '@customTypes/entities/well'
import { If, Then } from 'react-if'
import { getFlowmeterDetails } from 'api/getFlowmeterDetails'
import NoData from 'components/layouts/wells/NoData'
import useLocalStorage from 'hooks/useLocalStorage'

function WellsDaily() {
  const [currentWell, setCurrentWell] = React.useState<Well | undefined>(
    undefined,
  )
  const [wellList, setWellList] = React.useState<Wells>({})
  const [loadingWells, setLoadingWells] = React.useState(false)
  const [loadingContent, setLoadingContent] = React.useState(false)
  const { clientSelected, onSelectWell } = useContext(SettingsContext)
  const [userToken, setUserToken] = useLocalStorage('token', '')

  async function updateWell(
    newValue: number,
    defaultWell: Well | undefined,
    wells: Wells = wellList,
  ) {
    const wellMatched = findWell(wells, newValue) || defaultWell
    onSelectWell('')
    setLoadingContent(true)
    if (wellMatched) {
      const defaultFlowmeter = getFlowmeter(wellMatched)
      const details = await getFlowmeterDetails(
        defaultFlowmeter.id,
        String(userToken),
      )
      const newWell = { ...wellMatched }
      if (wellMatched.NMX) {
        newWell.NMX.rfc = details.rfc
        newWell.NMX.flowmeterImages = details.flowmeterImages
      }
      if (wellMatched.SIMTCA) {
        newWell.SIMTCA.rfc = details.rfc
        newWell.SIMTCA.flowmeterImages = details.flowmeterImages
      }

      setCurrentWell(newWell)
      onSelectWell(getFlowmeter(newWell).name)

      const date = new URLSearchParams(window.location.search).get('date')

      window.history.pushState(
        'well',
        'id',
        `/dashboard/wells?well=${newValue}${date ? `&date=${date}` : ''}`,
      )
    }

    return wellMatched
  }

  const handleChange = async (
    event: React.SyntheticEvent,
    newValue: String,
  ) => {
    await updateWell(Number(newValue), currentWell)
  }

  const getWells = async () => {
    setLoadingWells(true)
    setWellList({})
    setCurrentWell(undefined)
    const wells = await getWellsByClient(clientSelected, String(userToken))

    const wellsNames = Object.keys(wells).sort()
    const urlWellId = Number(
      new URLSearchParams(window.location.search).get('well'),
    )

    await updateWell(urlWellId, wells[wellsNames[0]], wells)

    setWellList(wells)
    setLoadingWells(false)
  }

  const renderTabList = () => {
    const wellNames = Object.keys(wellList).sort()

    const tabs = wellNames.map((wellName) => {
      const flowmetter = getFlowmeter(wellList[wellName])
      return (
        <Tab
          key={flowmetter.id}
          label={flowmetter.name}
          value={`${flowmetter.id}`}
        />
      )
    })

    if (!currentWell && Object.keys(wellList).length > 1)
      setCurrentWell(wellList[wellNames[0]])

    return tabs
  }

  useEffect(() => {
    if (userToken) {
      getWells()
    }
  }, [userToken, clientSelected])

  return (
    <Box sx={{ width: '100%', typography: 'body1', mt: 2 }}>
      <If condition={loadingWells}>
        <Then>
          <SkeletonTabs />
          <SkeletonTabContent />
        </Then>
      </If>
      <If condition={!loadingWells && Object.keys(wellList).length > 0}>
        <Then>
          <TabContext
            value={currentWell ? String(getFlowmeter(currentWell).id) : '0'}
          >
            <Box
              sx={{
                border: 1,
                p: 0.5,
                borderRadius: 2,
                borderColor: 'divider',
              }}
            >
              <TabList
                variant="scrollable"
                scrollButtons
                allowScrollButtonsMobile
                onChange={handleChange}
                aria-label="lab API tabs example"
              >
                {renderTabList()}
              </TabList>
            </Box>
            {currentWell ? (
              <PanelItem
                currentWell={currentWell}
                sx={{ mt: 4, ml: 1 }}
                loadingContent={loadingContent}
                setLoadingContent={setLoadingContent}
              />
            ) : (
              <SkeletonTabContent />
            )}
          </TabContext>
        </Then>
      </If>
      <If condition={!loadingWells && Object.keys(wellList).length <= 0}>
        <Then>
          <NoData />
        </Then>
      </If>
    </Box>
  )
}

export default withDashboard(WellsDaily, 'Pozos')

export const getServerSideProps = getSspWithSession(
  redirectIfNotAuth(PATH_LOGIN.root),
)
