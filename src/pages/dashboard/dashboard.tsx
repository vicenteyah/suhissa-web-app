import { Typography } from '@material-ui/core'
import {
  redirectIfNotAuth,
  redirectIfNotLoggedIn,
} from 'services/handlers/redirectIf'
import { PATH_LOGIN } from 'routes/paths'
import getSspWithSession from 'services/lib/getSspWithSession'
import { withDashboard } from 'components/HOC/withDashboard'
import { Card, Grid, useTheme } from '@mui/material'
import WellsLocation from 'components/layouts/dashboard/WellsLocation'
import { getFlowmeter } from '@customTypes/entities/well'
import Accordion from '@mui/material/Accordion'
import AccordionSummary from '@mui/material/AccordionSummary'
import AccordionDetails from '@mui/material/AccordionDetails'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore'
import AvailabilityChart from 'components/layouts/dashboard/general/AvailabilityChart'
import DataBox from 'components/molecules/DataBox'
import wells from 'services/data/getWells'
import moment from 'moment'
import 'moment/locale/es-mx'
import LineChart from 'components/layouts/dashboard/general/LineChart'
import { getWellsByClient } from 'api/wellsByClient'
import { useContext, useEffect, useState } from 'react'
import { SettingsContext } from 'contexts/SettingsContext'
import { Flowmeter } from '@customTypes/entities/Flowmeter'
import DashboardTable from 'components/layouts/wells/table/DashboardTable'
import useUserService from 'services/factories/UserService'
import { getAverageVolume } from 'api/getAverageVolume'

function Dashboard() {
  const [wellsLocation, setWellsLocation] = useState<Flowmeter[]>([])
  const { clientSelected, onSelectWell } = useContext(SettingsContext)
  const { logoutUser } = useUserService()
  const [averageVolume, setAverageVolume] = useState(0)
  const theme = useTheme()
  moment.locale('es-mx')

  const getWells = async (userToken: string) => {
    const wellsList = await getWellsByClient(clientSelected, String(userToken))
    const average = await getAverageVolume(clientSelected)
    setAverageVolume(Number(average) || 0)

    if (wellsList === 401) {
      redirectIfNotLoggedIn(PATH_LOGIN.root)
      logoutUser()
    }

    const wellsNames = Object.keys(wellsList).sort()
    const flowmeters = wellsNames.map((wellName) =>
      getFlowmeter(wellsList[wellName]),
    )
    setWellsLocation(flowmeters)
  }

  useEffect(() => {
    const token = JSON.parse(window.localStorage.getItem('token') || '')
    onSelectWell('')
    if (token) getWells(token)
  }, [clientSelected])

  return (
    <>
      <Typography variant="h3" component="h1" sx={{ mt: 4 }}>
        Dashboard
      </Typography>
      <Grid container spacing={2} sx={{ mt: 2 }}>
        <Grid item md={4} xs={12} sm={5}>
          <Card sx={{ p: 2 }}>
            <AvailabilityChart
              title="Disponibilidad General"
              volume={wells.reduce(
                (volume, well) => volume + well.consesion.volume,
                0,
              )}
              availability={wells.reduce(
                (volume, well) => volume + well.availabilty,
                0,
              )}
            />
          </Card>
        </Grid>
        <Grid item md={8} xs={12} sm={7}>
          <Card sx={{ p: 2 }}>
            <LineChart
              title="Volumen Consumido Total"
              titleAlign="left"
              series={[1150000, 3040000, 5175000, 5725000, 6460000]}
              categories={moment.monthsShort()}
              seriesLabel="Acumulado"
              annotationColor={theme.palette.error.light}
            />
          </Card>
        </Grid>
      </Grid>
      <Grid container spacing={2} sx={{ mt: 2 }}>
        <Grid item xs={12} sm={6} lg={3}>
          <DataBox
            title="Volumen Diario Promedio"
            value={averageVolume}
            dataUnity="m3"
          />
        </Grid>
        <Grid item xs={12} sm={6} lg={3}>
          <DataBox
            title="Suma totalizadores"
            value={4266496}
            dataUnity="m3"
            total={10726496}
          />
        </Grid>
        <Grid item xs={12} sm={6} lg={3}>
          <DataBox
            title="Concesión por acumular"
            value={6460000}
            dataUnity="m3"
            total={10726496}
          />
        </Grid>
        <Grid item xs={12} sm={6} lg={3}>
          <DataBox
            title="Concesiones autorizadas"
            value={10726496}
            dataUnity="m3"
            total={10726496}
          />
        </Grid>
      </Grid>
      <Accordion defaultExpanded sx={{ mt: 3 }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel1a-header"
        >
          <Typography variant="h5">Ubicación</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <WellsLocation
            wells={wellsLocation}
            initialLatitude={23.6260333}
            initialLongitude={-102.5375005}
            showPopUp
            height="70vh"
          />
        </AccordionDetails>
      </Accordion>
      <Accordion defaultExpanded sx={{ mt: 3 }}>
        <AccordionSummary
          expandIcon={<ExpandMoreIcon />}
          aria-controls="panel1a-content"
          id="panel2a-header"
        >
          <Typography variant="h5">Listado de sitios</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <DashboardTable wells={wellsLocation} />
        </AccordionDetails>
      </Accordion>
    </>
  )
}

export default withDashboard(Dashboard, 'Dashboard')

export const getServerSideProps = getSspWithSession(
  redirectIfNotAuth(PATH_LOGIN.root),
)
