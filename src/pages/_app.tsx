import 'simplebar/src/simplebar.css'

import 'react-quill/dist/quill.snow.css'
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css'
import 'react-image-lightbox/style.css'
import Head from 'next/head'
import { AppProps } from 'next/app'
import { CacheProvider, EmotionCache } from '@emotion/react'

import { SettingsProvider } from 'contexts/SettingsContext'

import ThemeConfig from 'theme'

import createEmotionCache from 'utils/functions/createEmotionCache'

import LoadingScreen from 'components/molecules/LoadingScreen'
import TopProgressBar from 'components/atoms/TopProgressBar'
import ThemePrimaryColor from 'components/layouts/ThemePrimaryColor'
import NotistackProvider from 'components/layouts/NotistackProvider'

const clientSideEmotionCache = createEmotionCache()

interface MyAppProps extends AppProps {
  emotionCache?: EmotionCache
}

export default function MyApp(props: MyAppProps) {
  const { Component, emotionCache = clientSideEmotionCache, pageProps } = props

  return (
    <SettingsProvider>
      <CacheProvider value={emotionCache}>
        <Head>
          <meta name="viewport" content="initial-scale=1, width=device-width" />
        </Head>

        <ThemeConfig>
          <ThemePrimaryColor>
            <NotistackProvider>
              <LoadingScreen />
              <TopProgressBar />
              <Component {...pageProps} />
            </NotistackProvider>
          </ThemePrimaryColor>
        </ThemeConfig>
      </CacheProvider>
    </SettingsProvider>
  )
}
