import { motion } from 'framer-motion'
import NextLink from 'next/link'
import { styled } from '@material-ui/core/styles'
import { Box, Button, Typography, Container } from '@material-ui/core'
import LogoOnlyLayout from 'components/layouts/LogoOnlyLayout'
import { MotionContainer, varBounceIn } from 'components/animate'
import Page from 'components/layouts/Page'
import { PageNotFoundIllustration } from 'assets'

const RootStyle = styled(Page)(({ theme }) => ({
  display: 'flex',
  minHeight: '100%',
  alignItems: 'center',
  paddingTop: theme.spacing(15),
  paddingBottom: theme.spacing(10),
}))

export default function PageNotFound() {
  return (
    <LogoOnlyLayout>
      <RootStyle title="Página no encontrada | SITMCO">
        <Container>
          <MotionContainer initial="initial" open>
            <Box sx={{ maxWidth: 480, margin: 'auto', textAlign: 'center' }}>
              <motion.div variants={varBounceIn}>
                <Typography variant="h3" paragraph>
                  Lo sentimos, ¡Página No encontrada!
                </Typography>
              </motion.div>
              <Typography sx={{ color: 'text.secondary' }}>
                Lo sentimos, no pudimos encontrar la página que está buscando.
                Quizás ¿Ha escrito mal la URL? Asegúrese de revisar su
                ortografía.
              </Typography>

              <motion.div variants={varBounceIn}>
                <PageNotFoundIllustration
                  sx={{ height: 260, my: { xs: 5, sm: 10 } }}
                />
              </motion.div>

              <NextLink href="/">
                <Button size="large" variant="contained">
                  Regresar al inicio
                </Button>
              </NextLink>
            </Box>
          </MotionContainer>
        </Container>
      </RootStyle>
    </LogoOnlyLayout>
  )
}
