import withSession from 'services/lib/session'

export default withSession(async (req, res) => {
  const user = req.session.get('user')

  if (user) {
    res.json({
      ...user,
      isLoggedIn: true,
    })
  } else {
    res.json({
      isLoggedIn: false,
    })
  }
})
