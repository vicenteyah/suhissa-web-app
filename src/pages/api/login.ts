import UserSession from '@customTypes/dto/UserSession'
import { Client, mutateClients } from '@customTypes/entities/client'
import { Method } from 'services/lib/fetchFromAPI'
import fetchJson from 'services/lib/fetchJson'
import withSession from 'services/lib/session'

interface IUserRequest extends Body {
  username: string
  password: string
}

const sortClients = (firstClient: Client, secondClient: Client) => {
  let value = 0
  if (firstClient.name < secondClient.name) {
    value = -1
  } else if (firstClient.name > secondClient.name) {
    value = 1
  }
  return value
}

export default withSession(async (req, res) => {
  const { username, password } = (await req.body) as IUserRequest
  const baseUrl =
    process.env.NEXT_PUBLIC_API_URL || `http://demo.localhost:8000/api/`

  try {
    const URL = `${baseUrl}sessions_simtca/`
    const body = JSON.stringify({ email: username, password })

    process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'

    const userData = await fetchJson(URL, {
      method: Method.POST,
      headers: { 'Content-Type': 'application/json' },
      body,
    })

    const clients = mutateClients(JSON.stringify(userData.user.clients))
    const clientsSorted = clients.sort(sortClients)

    const rawUser: UserSession = {
      isLoggedIn: true,
      userData: {
        name: userData.user.name,
        username: userData.user.email,
        clients: clientsSorted,
        hasChangedPassword: true,
      },
      token: userData.token,
    }
    req.session.set('user', rawUser)
    await req.session.save()
    res.json(rawUser)
  } catch (error: any) {
    if (error.response) {
      res.status(error.response.status || 500).json(error.data)
    } else {
      res.status(500).json(error.data)
    }
  }
})
