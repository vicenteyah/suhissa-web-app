import { NextApiRequest, NextApiResponse } from 'next'
import DB from 'services/data/SimulateApi'

const allWells = async (req: NextApiRequest, res: NextApiResponse) => {
  const db = new DB()
  const data = await db.getAll()
  res.status(200).json(data)
}

export default allWells
