import { NextApiRequest, NextApiResponse } from 'next'
import DB from 'services/data/SimulateApi'

const getOneWell = async (req: NextApiRequest, res: NextApiResponse) => {
    const { id } = req.query
    const db = new DB()
    const data = await db.getOne(Number(id))
    return  res.status(200).json(data)
}

export default getOneWell