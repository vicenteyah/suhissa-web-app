import fetchFromAPI, { FetchFromAPIProps } from 'services/lib/fetchFromAPI'
import handleFetchError from 'services/lib/handleFetchError'
import withSession from 'services/lib/session'
import getProperty from 'utils/objects/getProperty'

export default withSession((req, res) => {
  const request: FetchFromAPIProps = req.body
  return fetchFromAPI(req)(request)
    .then(getProperty('results'))
    .then(res.json)
    .catch(handleFetchError(res))
})
