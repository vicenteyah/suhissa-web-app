import { PATH_DASHBOARD } from 'routes/paths'
import { redirectIfLoggedIn } from 'services/handlers/redirectIf'
import getSspWithSession from 'services/lib/getSspWithSession'
import { default as Login } from './Login'

export default Login

export const getServerSideProps = getSspWithSession(
  redirectIfLoggedIn(PATH_DASHBOARD.root),
)
