import React, { useContext, useState } from 'react'
import { Box, Stack, Container, Typography } from '@material-ui/core'
import { MHidden } from 'components/@material-extend'
import LoginForm from 'components/molecules/LoginForm'
import { useForm } from 'react-hook-form'
import { yupResolver } from '@hookform/resolvers/yup'
import * as Yup from 'yup'
import { IUserLogin } from '@customTypes/entities/user'
import {
  RootStyle,
  SectionStyle,
  ContentStyle,
  LogoImage,
  WelcomeTitle,
  WelcomeParagraph,
  BackgroundImage,
  Footer,
} from 'components/atoms/Login'
import useSnackbar from 'hooks/useSnackbar'
import { useRouter } from 'next/router'
import { PATH_DASHBOARD } from 'routes/paths'
import useUserService from 'services/factories/UserService'
import useLocalStorage from 'hooks/useLocalStorage'
import { SettingsContext } from 'contexts/SettingsContext'
import { mutateClients } from '@customTypes/entities/client'

const LoginSchema: Yup.SchemaOf<IUserLogin> = Yup.object().shape({
  username: Yup.string().required(
    "El campo 'correo electrónico' es obligatorio",
  ),
  password: Yup.string().required("El campo 'contraseña' es obligatorio"),
})

function Login() {
  const { signInUser } = useUserService()
  const { enqueueSuccessSnackbar, enqueueErrorSnackbar } = useSnackbar()
  const [showPassword, setShowPassword] = useState(false)
  const { clientSelected, onSelectClient } = useContext(SettingsContext)
  const [clients, setClients] = useLocalStorage('clients', [])
  const [token, setToken] = useLocalStorage('token', '')

  const router = useRouter()
  const {
    control,
    handleSubmit,
    formState: { errors, isSubmitting },
  } = useForm({
    resolver: yupResolver(LoginSchema),
  })

  const onSubmit = handleSubmit(async (data: IUserLogin | any) => {
    try {
      const response = await signInUser(data)

      setToken(response?.token || '')

      const newListClients = response?.userData.clients || []

      const selectedIsInClients = newListClients.some(
        (client) => client.id === clientSelected,
      )

      if (!selectedIsInClients && newListClients.length > 0) {
        onSelectClient(newListClients[0])
      }
      setClients(newListClients)
      enqueueSuccessSnackbar({ message: 'Bienvenido al sistema' })
      router.push(PATH_DASHBOARD.root)
    } catch (error: any) {
      if (error.response && error.response.status === 401) {
        enqueueErrorSnackbar({
          message:
            'Correo electrónico o contraseña incorrectos, por favor inténtelo de nuevo.',
        })
      } else {
        enqueueErrorSnackbar({
          message: 'Ocurrió un error inesperado. Inténtelo más tarde. ',
        })
      }
    }
  })

  function handleShowPassword() {
    return setShowPassword((previous) => !previous)
  }

  return (
    <RootStyle title="Acceso a Clientes">
      <MHidden width="mdDown">
        <SectionStyle>
          <WelcomeTitle variant="h3">BIENVENIDO</WelcomeTitle>
          <WelcomeParagraph variant="h5">
            A la Plataforma de Gestión de Indicadores <br /> y Consumos
          </WelcomeParagraph>
          <BackgroundImage />
        </SectionStyle>
      </MHidden>

      <Container maxWidth="sm">
        <ContentStyle>
          <LogoImage component="img" src="/static/brand/logo_full.png" />
          <Stack direction="row" alignItems="center" sx={{ mb: 5 }}>
            <Box sx={{ flexGrow: 1 }}>
              <Typography variant="h4" gutterBottom>
                Acceso a Clientes
              </Typography>
              <Typography sx={{ color: 'text.secondary' }}>
                A continuación ingresa tus datos de acceso.
              </Typography>
            </Box>
          </Stack>
          <LoginForm
            showPassword={showPassword}
            handleShowPassword={handleShowPassword}
            onSubmit={onSubmit}
            isSubmitting={isSubmitting}
            control={control}
            errors={errors}
          />

          <Footer>
            Powered by{' '}
            <a
              href="https://suhissa.com.mx"
              style={{ color: '#4a5fa4' }}
              target="_blank"
              rel="noreferrer"
            >
              SUHISSA
            </a>
            {' | '}
            <a
              href="https://suhissa.com.mx/contacto/"
              style={{ color: '#4a5fa4' }}
              target="_blank"
              rel="noreferrer"
            >
              Soporte
            </a>
          </Footer>
        </ContentStyle>
      </Container>
    </RootStyle>
  )
}

export default Login
