import { PATH_DASHBOARD, PATH_LOGIN } from 'routes/paths'
import getSspWithSession from 'services/lib/getSspWithSession'
import HeaderBreadcrumbs from 'components/molecules/HeaderBreadcrumbs'
import { withDashboard } from 'components/HOC/withDashboard'
import { withTabs } from 'components/HOC/withTabs'
import { redirectIfNotAuth } from 'services/handlers/redirectIf'
import { Typography } from '@material-ui/core'
import { Box } from '@mui/material'

function Account() {
  return (
    <>
      <Typography variant="h3" component="h1" paragraph>
        Cuenta
      </Typography>
      <Box sx={{ height: '70vh' }} />
    </>
  )
}

export default withDashboard(Account, 'Cuenta')

export const getServerSideProps = getSspWithSession(
  redirectIfNotAuth(PATH_LOGIN.root),
)
