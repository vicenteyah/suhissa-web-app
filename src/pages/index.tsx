import { PATH_LOGIN } from 'routes/paths'
import { default as Dashboard } from './dashboard/wells'

export default Dashboard

export const getServerSideProps = () => {
  return {
    redirect: {
      destination: PATH_LOGIN.root,
      permanent: false,
    },
  }
}
