# SUHISSA frontend

## Requirements

[node ^16](https://nodejs.org/es/)

## Installation

`npm install`

## Run development version

`npm run dev`

Server will run in [8222](localhost:8222) port

## Compile Production version

`npm run build`
